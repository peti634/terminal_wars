package main

import (
	"terminal_wars/internal/gui"
)

func main() {
	gui.GuiInit()
	gui.GuiAppRun()
}
