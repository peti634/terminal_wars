package info

var (
	infoFunc func(s string)
)

func Info(s string) {
	infoFunc(s)
}

func SetInfoFunc(f func(s string)) {
	infoFunc = f
}
