package formater

import "strconv"

func TimeFormat(time int) string {
	time /= 1000
	sec := time % 60
	min := (time / 60) % 60
	hour := (time / 3600)
	ret := ""
	if hour > 0 {
		if hour < 10 {
			ret += "0"
		}
		ret += strconv.Itoa(hour) + ":"
	}
	if min > 0 {
		if min < 10 {
			ret += "0"
		}
		ret += strconv.Itoa(min) + ":"
	}
	if sec < 10 {
		ret += "0"
	}

	return ret + strconv.Itoa(sec)
}

func ValueFormat(v int) string {
	if v >= 10*1000*1000 {
		return strconv.Itoa(v/(1000*1000)) + "m"
	}
	if v >= 10000 {
		return strconv.Itoa(v/1000) + "k"
	}
	return strconv.Itoa(v)
}
