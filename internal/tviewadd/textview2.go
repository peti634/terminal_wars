package tviewadd

import (
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type TextView2 struct {
	*tview.TextView

	textDrawFunc func(tv2 *TextView2)
}

func NewTextView2() *TextView2 {
	return &TextView2{
		TextView: tview.NewTextView(),
	}
}

func (t *TextView2) Draw(screen tcell.Screen) {
	if t.textDrawFunc != nil {
		t.textDrawFunc(t)
	}
	t.TextView.Draw(screen)
}

func (t *TextView2) SetTextDrawFunc(f func(tv2 *TextView2)) *TextView2 {
	t.textDrawFunc = f
	return t
}
func (t *TextView2) SetDynamicColors(b bool) *TextView2 {
	t.TextView.SetDynamicColors(b)
	return t
}
