package game

func initArmydef() {
	///////////////////////////DEF//////////////////////////

	ProductMap["Raw ammo"] = &Product{
		CostTime: 5000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         1,
				ProductName: "Gunpowder",
			},
			ProductCost{
				Num:         1,
				ProductName: "Iron plate",
			},
		},
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Defense builder"],
		},
		NeedDep: "DEP_BUILD_DEF",
	}

	ProductMap["Wall repair"] = &Product{
		CostTime: 5000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         1,
				ProductName: "Coal",
			},
			ProductCost{
				Num:         1,
				ProductName: "Iron plate",
			},
		},
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Defense builder"],
		},
		NeedDep: "DEP_RESC_DEFWALL",
	}
	ProductMap["Turret"] = &Product{
		CostTime: 120000,
		CreateCost: []ProductCost{
			ProductCost{
				ProductName: "Iron plate",
				Num:         100,
			},
			ProductCost{
				ProductName: "Copper plate",
				Num:         100,
			},
			ProductCost{
				ProductName: "Copper cable",
				Num:         10,
			},
		},
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Defense builder"],
		},
		NeedDep: "DEP_RESC_DEFTURR",
	}
	ProductMap["Turret ammo"] = &Product{
		CostTime: 30000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         10,
				ProductName: "Raw ammo",
			},
			ProductCost{
				Num:         1,
				ProductName: "Iron plate",
			},
		},
		UnlimitStore: true,
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Defense builder"],
		},
		NeedDep: "DEP_RESC_DEFFAMMO",
	}
	///////////////////////////ARMY//////////////////////////
	ProductMap["Robot warrior"] = &Product{
		CostTime: 30000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         1,
				ProductName: "Iron block",
			},
			ProductCost{
				Num:         4,
				ProductName: "Circuit",
			},
			ProductCost{
				Num:         10,
				ProductName: "Raw ammo",
			},
		},
		UnlimitStore: true,
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Army factory"],
		},
		Army: &ProductArmy{
			Priority: 2,
			Health:   400,
			Damage:   4,
		},
		AI:      AIDetails{PointCost: 500 * 1000},
		NeedDep: "DEP_RESC_ARMYROBOT",
	}

	ProductMap["Tank"] = &Product{
		CostTime: 100000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         2,
				ProductName: "Iron block",
			},
			ProductCost{
				Num:         10,
				ProductName: "Engine",
			},
			ProductCost{
				Num:         20,
				ProductName: "Raw ammo",
			},
		},
		UnlimitStore: true,
		Producer:     &ProductProducerDetails{FreeProducer: FreeProducerMap["Army factory"]},
		Army: &ProductArmy{
			Health:   2000,
			Damage:   6,
			Priority: 3,
		},
		AI:      AIDetails{PointCost: 5 * 1000 * 1000},
		NeedDep: "DEP_RESC_TANK",
	}
	ProductMap["Generator"] = &Product{
		CostTime: 10,
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Army factory"],
		},
		Army: &ProductArmy{
			Health: 100,
			Damage: 100,
		},
		NeedDep: "DEP_HIDDEN",
	}
	ProductMap["Artillery"] = &Product{
		CostTime: 10,
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Army factory"],
		},
		Army:    &ProductArmy{Health: 100, Damage: 100},
		NeedDep: "DEP_HIDDEN",
	}
	ProductMap["Buffer"] = &Product{
		CostTime: 10,
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Army factory"],
		},
		Army:    &ProductArmy{Health: 100, Damage: 100},
		NeedDep: "DEP_HIDDEN",
	}

}
