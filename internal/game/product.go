package game

import "math"

type ProductCost struct {
	Num         int
	CachedStore *Store
	ProductName string
}
type ProductBuilding struct {
	DepResolver string
	ProducerAdd *Producer

	StoreName string
	StoreAdd  int
}

type ProductCostIncreaser struct {
	PointerProduct    *Product
	BuildCounter      int
	OriginalCostValue map[string]int
	IncFactor         int
	IncStep           int
	DecFactor         int
}

type ProductProducerDetails struct {
	FreeProducer *Producer
}

type ProductResearch struct {
	ShowText      string
	DepResolver   string
	CanReResearch bool

	//faster resch
	FasterProducerName string

	//cheaper resch
	CheaperProductName string

	//faster or cheaper factor value
	Factor int

	//other special
	FinishedFunc func(*Product)
}

type ProductArmy struct {
	Health   int
	Damage   int
	Priority int
}

type AIDetails struct {
	PointCost int
}

type ProductFile struct {
	CostIncBuildCounter int `json:",omitempty"`
	CostIncDecFactor    int `json:",omitempty"`
}

type Product struct {
	Name string

	CostTime     int
	CreateCost   []ProductCost
	UnlimitStore bool

	NeedDep string

	AddFunc func(num int)
	//func in menu
	SelectFunc func()
	ClickFunc  func()
	//if it is building, otherwise nil
	Building *ProductBuilding

	Producer *ProductProducerDetails

	Research *ProductResearch

	Army          *ProductArmy
	CostIncreaser ProductCostIncreaser
	AI            AIDetails
}

func (b *ProductCostIncreaser) RecalculateCost() {
	p := b.PointerProduct
	if b.IncStep == 0 {
		return
	}

	factor := math.Pow(1+float64(b.IncFactor)/100, float64(b.BuildCounter/b.IncStep))
	if b.DecFactor > 0 {
		factor /= float64(100+b.DecFactor) / 100
	}

	for i := range p.CreateCost {
		originalValue := b.OriginalCostValue[p.CreateCost[i].ProductName]
		p.CreateCost[i].Num = int(float64(originalValue) * factor)
	}
}

func (b *ProductCostIncreaser) StoreOriginalCost(p *Product) {
	b.PointerProduct = p

	if b.IncStep != 0 && b.IncFactor != 0 {
		//store original cost value
		b.OriginalCostValue = make(map[string]int)
		for _, c := range p.CreateCost {
			b.OriginalCostValue[c.ProductName] = c.Num
		}
		p.CostIncreaser.RecalculateCost()
	}
}

func (b *ProductCostIncreaser) Buy() {
	b.BuildCounter += 1
	b.RecalculateCost()
}

func (b *ProductCostIncreaser) AddDecValue(dec int) {
	b.DecFactor += dec
	b.RecalculateCost()
}

func (p *Product) Reset() {
	p.CostIncreaser.BuildCounter = 0
	p.CostIncreaser.DecFactor = 0
	p.CostIncreaser.RecalculateCost()
}

func (p *Product) SaveData() ProductFile {
	return ProductFile{
		CostIncBuildCounter: p.CostIncreaser.BuildCounter,
		CostIncDecFactor:    p.CostIncreaser.DecFactor,
	}
}

func (p *Product) LoadData(f ProductFile) {
	p.CostIncreaser.BuildCounter = f.CostIncBuildCounter
	p.CostIncreaser.DecFactor = f.CostIncDecFactor
	p.CostIncreaser.RecalculateCost()
}
