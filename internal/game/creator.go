package game

import (
	"strconv"

	"terminal_wars/internal/info"
)

type CreatorFile struct {
	Queue     []string
	BuildTime int
	Factor    int
}

type Creator struct {
	queue     []*Product
	buildTime int
	factor    int
}

func NewCreator() *Creator {
	return &Creator{factor: 100}
}

func (b *Creator) Reset() {
	defCreator := NewCreator()
	b.queue = defCreator.queue
	b.buildTime = defCreator.buildTime
	b.factor = defCreator.factor
}

func (b *Creator) SaveData() CreatorFile {

	ret := CreatorFile{
		BuildTime: b.buildTime,
		Factor:    b.factor,
		Queue:     []string{},
	}
	for _, i := range b.queue {
		ret.Queue = append(ret.Queue, i.Name)
	}
	return ret
}

func (b *Creator) LoadData(f CreatorFile) {
	b.factor = f.Factor
	b.buildTime = f.BuildTime

	queue := []*Product{}
	for _, item := range f.Queue {
		queue = append(queue, ProductMap[item])
	}
	b.queue = queue
}

func (b *Creator) BuyAndAddQueue(p *Product) {

	if p.Research != nil {
		if p.Research.DepResolver != "" && CheckDep(p.Research.DepResolver) {
			return
		}

		//check multiple add
		for _, i := range b.queue {
			if i == p {
				return
			}
		}
	}

	for _, i := range p.CreateCost {
		if i.CachedStore.CheckStore(i.Num) == false {
			return
		}
	}

	for _, i := range p.CreateCost {
		i.CachedStore.Add(-i.Num)
	}
	b.queue = append(b.queue, p)
	p.CostIncreaser.Buy()

}

func (b *Creator) createDoIt(p *Product) {
	//check building
	if p.Building != nil {
		building := p.Building
		if building.DepResolver != "" {
			DepResolve(building.DepResolver)
		}
		if building.ProducerAdd != nil {
			building.ProducerAdd.Add(1)
		} else if building.StoreName != "" {

			producerList := SearchProductNameByBuildingName(building.StoreName)
			for _, pname := range producerList {
				store := StoreMap[pname]
				store.AddMax(building.StoreAdd)
			}
		} else {
			// TODO: error
		}

	}
	if p.Research != nil {

		if p.Research.DepResolver != "" {
			DepResolve(p.Research.DepResolver)
		}
		if p.Research.FasterProducerName != "" {

			producerList := SearchProductNameByBuildingName(p.Research.FasterProducerName)
			for _, pname := range producerList {
				producer := ProducerMap[pname]
				producer.AddFactor(p.Research.Factor)
			}

		}

		if p.Research.CheaperProductName != "" {
			product := ProductMap[p.Research.CheaperProductName]
			product.CostIncreaser.AddDecValue(p.Research.Factor)
		}

		if p.Research.FinishedFunc != nil {
			p.Research.FinishedFunc(p)
		}
		info.Info("Research finished: " + p.Name)
	}
}

func (b *Creator) GetCostTime() int {
	p := b.queue[0]
	pCostTime := p.CostTime
	if p.Research != nil && p.Research.CanReResearch {
		pCostTime *= p.CostIncreaser.BuildCounter
	}
	return int(float32(pCostTime) / float32(b.factor/100))
}

func (b *Creator) Calc() {
	if len(b.queue) > 0 {
		b.buildTime += 100
		p := b.queue[0]
		costTime := b.GetCostTime()
		if b.buildTime >= costTime {
			b.createDoIt(p)
			if len(b.queue) > 1 {
				b.queue = b.queue[1:]
				b.buildTime -= costTime
			} else {
				b.queue = []*Product{}
				b.buildTime = 0
			}
		}
	}
}
func (b *Creator) GetInfo() string {
	if len(b.queue) > 0 {
		p := b.queue[0]

		var plus string
		if len(b.queue) > 1 {
			plus = " (+" + strconv.Itoa(len(b.queue)-1) + ")"
		}
		return p.Name + " " +
			strconv.Itoa(b.buildTime*100/b.GetCostTime()) + "%" + plus

	}
	return ""
}

func (b *Creator) AddFactor(factor int) {
	b.factor += factor
}
