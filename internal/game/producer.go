package game

import (
	"fmt"
	"strconv"
	"terminal_wars/internal/formater"
)

type ProducerFile struct {
	Factor           int  `json:",omitempty"`
	ProducerCount    int  `json:",omitempty"`
	PartProduct      int  `json:",omitempty"`
	ResHasTaken1Cost bool `json:",omitempty"`
}
type Producer struct {
	factor           int
	ProducerCount    int
	partProduct      int
	refStore         *Store
	refFreeProducer  *Producer
	resHasTaken1Cost bool
	cacheProduct     *Product
	processing       bool
}

func NewProducer(refStore *Store, refFreeProducer *Producer) *Producer {
	var cacheProduct *Product = nil
	if refStore != nil {
		cacheProduct = refStore.GetProduct()
	}

	return &Producer{
		factor:          0,
		refStore:        refStore,
		refFreeProducer: refFreeProducer,
		cacheProduct:    cacheProduct,
	}
}

func (m *Producer) Reset() {
	defProducer := NewProducer(m.refStore, m.refFreeProducer)

	m.factor = defProducer.factor
	m.ProducerCount = defProducer.ProducerCount
	m.partProduct = defProducer.partProduct
	m.resHasTaken1Cost = defProducer.resHasTaken1Cost
}

func (m *Producer) SaveData() ProducerFile {
	return ProducerFile{
		Factor:           m.factor,
		ProducerCount:    m.ProducerCount,
		PartProduct:      m.partProduct,
		ResHasTaken1Cost: m.resHasTaken1Cost,
	}
}

func (m *Producer) LoadData(f ProducerFile) {
	m.factor = f.Factor
	m.ProducerCount = f.ProducerCount
	m.partProduct = f.PartProduct
	m.resHasTaken1Cost = f.ResHasTaken1Cost
}

func (m *Producer) GetProducerNum() int {
	return m.ProducerCount
}

func (m *Producer) GetFreeProducerNum() int {
	return m.refFreeProducer.GetProducerNum()
}

func (m *Producer) GetProduct() *Product {
	return m.cacheProduct
}

func (m *Producer) tryTakeCost(num int) int {
	p := m.cacheProduct

	for _, c := range p.CreateCost {
		sum := c.Num * num
		storeCount := c.CachedStore.GetCount()
		if storeCount > sum {
			continue
		} else {
			num = storeCount / c.Num
			if num == 0 {
				return 0
			}
		}
	}

	for _, c := range p.CreateCost {
		sum := c.Num * num
		c.CachedStore.Add(-sum)
	}

	return num
}

func (m *Producer) tryTakeFirstCost() {
	if m.refStore.GetFreeSpace() > 0 && m.tryTakeCost(1) == 1 {
		m.resHasTaken1Cost = true
	} else {
		m.partProduct = 0
	}
}
func (m *Producer) CalcReqTime() int {
	if m.cacheProduct == nil {
		return 1
	}
	return (m.cacheProduct.CostTime * 1000) / (m.factor + 1000)
}

func (m *Producer) CalcProducer() {
	if m.cacheProduct == nil {
		return
	}

	reqTime := m.CalcReqTime()
	m.processing = false
	if m.ProducerCount > 0 && m.partProduct == 0 && m.resHasTaken1Cost == false {
		m.tryTakeFirstCost()
	}

	if m.resHasTaken1Cost {
		m.partProduct += 100 * m.ProducerCount
		m.processing = true
		if m.partProduct >= reqTime {

			m.refStore.Add(1)
			m.resHasTaken1Cost = false
			m.partProduct -= reqTime

			count := m.partProduct / reqTime
			if count > 0 {
				free := m.refStore.GetFreeSpace()
				if free < count {
					count = free
				}
				count = m.tryTakeCost(count)

				if m.refStore.Add(count) == false {
					//Store is not full
					m.partProduct = m.partProduct - (count * reqTime)

				}
			}
			if m.partProduct > 0 {
				m.tryTakeFirstCost()
			}
		}

	}

}

func (m *Producer) GetInfo() string {
	return strconv.Itoa((m.partProduct * 100) / m.CalcReqTime())
}

func (m *Producer) GetMoreInfo() string {
	var ret string
	calcTime := m.CalcReqTime()
	if m.processing && calcTime > 0 {
		prodSpeed := float32(1000*m.ProducerCount) / float32(calcTime)
		if prodSpeed >= 1 {
			if prodSpeed >= 100 {
				ret = "+" + formater.ValueFormat(int(prodSpeed)) + "/s"
			} else {
				ret = "+" + fmt.Sprintf("%.1f", prodSpeed) + "/s"
			}

		} else {
			ret = strconv.Itoa((m.partProduct*100)/calcTime) + "%"
		}

	}
	if m.refStore != nil {
		return m.refStore.GetInfo() + " " + ret
	}
	return ret
}

func (m *Producer) Add(num int) {
	m.ProducerCount += num
}
func (m *Producer) AddProducer(num int) {
	if m.refFreeProducer == nil {
		m.ProducerCount += num
	} else {
		if m.refFreeProducer.ProducerCount < num {
			num = m.refFreeProducer.ProducerCount
		}

		m.refFreeProducer.ProducerCount -= num
		m.ProducerCount += num

	}
}

func (m *Producer) DecProducer(num int) {
	if m.ProducerCount < num {
		num = m.ProducerCount
	}
	m.refFreeProducer.ProducerCount += num
	m.ProducerCount -= num

}

func (m *Producer) AddFactor(factor int) {
	m.factor += factor * 10
}
