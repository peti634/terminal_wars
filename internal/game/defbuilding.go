package game

func createBuildingProduct(costTime int, pc []ProductCost, b *ProductBuilding) *Product {
	return &Product{
		CostTime:      costTime,
		CreateCost:    pc,
		Building:      b,
		CostIncreaser: ProductCostIncreaser{IncStep: 10, IncFactor: 10},
	}
}
func initBuildings() {
	/////////////////////////////BUILDINGS//////////////
	ProductMap["Mine"] = createBuildingProduct(2000,
		[]ProductCost{
			ProductCost{
				ProductName: "Iron",
				Num:         10,
			},
			ProductCost{
				ProductName: "Copper",
				Num:         10,
			},
		},
		&ProductBuilding{
			DepResolver: "DEP_BUILD_MINE",
			ProducerAdd: FreeProducerMap["Mine"],
		})

	ProductMap["Furnace"] = &Product{
		CostTime: 5000,
		CreateCost: []ProductCost{
			ProductCost{
				ProductName: "Iron",
				Num:         40,
			},
			ProductCost{
				ProductName: "Copper",
				Num:         25,
			},
		},
		NeedDep: "DEP_BUILD_MINE",
		Building: &ProductBuilding{
			DepResolver: "DEP_BUILD_FURNACE",
			ProducerAdd: FreeProducerMap["Furnace"],
		},
		CostIncreaser: ProductCostIncreaser{IncStep: 10, IncFactor: 15},
	}
	ProductMap["Factory"] = &Product{
		CostTime: 6000,
		CreateCost: []ProductCost{
			ProductCost{
				ProductName: "Iron plate",
				Num:         50,
			},
			ProductCost{
				ProductName: "Copper plate",
				Num:         30,
			},
		},
		NeedDep: "DEP_BUILD_FURNACE",
		Building: &ProductBuilding{
			DepResolver: "DEP_BUILD_FACTORY",
			ProducerAdd: FreeProducerMap["Factory"],
		},
		CostIncreaser: ProductCostIncreaser{IncStep: 5, IncFactor: 4},
	}

	ProductMap["Advanced factory"] = &Product{
		CostTime: 10000,
		CreateCost: []ProductCost{
			ProductCost{
				ProductName: "Iron plate",
				Num:         100,
			},
			ProductCost{
				ProductName: "Copper plate",
				Num:         40,
			},
		},
		NeedDep: "DEP_RESC_ADVFACTORY",
		Building: &ProductBuilding{
			DepResolver: "DEP_BUILD_ADVFACTORY",
			ProducerAdd: FreeProducerMap["Advanced factory"],
		},
		CostIncreaser: ProductCostIncreaser{IncStep: 5, IncFactor: 25},
	}

	ProductMap["Defense builder"] = &Product{
		CostTime: 12000,
		CreateCost: []ProductCost{
			ProductCost{
				ProductName: "Iron plate",
				Num:         120,
			},
			ProductCost{
				ProductName: "Bulb",
				Num:         30,
			},
		},
		Building: &ProductBuilding{
			DepResolver: "DEP_BUILD_DEF",
			ProducerAdd: FreeProducerMap["Defense builder"],
		},
		CostIncreaser: ProductCostIncreaser{IncStep: 5, IncFactor: 20},
	}

	ProductMap["Army factory"] = &Product{
		CostTime: 20000,
		CreateCost: []ProductCost{
			ProductCost{
				ProductName: "Iron plate",
				Num:         210,
			},
			ProductCost{
				ProductName: "Silicon plate",
				Num:         80,
			},
		},
		Building: &ProductBuilding{
			DepResolver: "DEP_BUILD_ARMY",
			ProducerAdd: FreeProducerMap["Army factory"],
		},
		CostIncreaser: ProductCostIncreaser{IncStep: 2, IncFactor: 20},
	}
	//////////////////////////STORE//////////////////////
	ProductMap["Mine store"] = &Product{
		CostTime: 6000,
		CreateCost: []ProductCost{
			ProductCost{
				ProductName: "Iron plate",
				Num:         10,
			},
			ProductCost{
				ProductName: "Screw",
				Num:         100,
			},
		},
		NeedDep: "DEP_RESC_STORE",
		Building: &ProductBuilding{
			StoreName: "Mine",
			StoreAdd:  1000,
		},
		CostIncreaser: ProductCostIncreaser{IncStep: 1, IncFactor: 20},
	}

	ProductMap["Furnace store"] = &Product{
		CostTime: 6000,
		CreateCost: []ProductCost{
			ProductCost{
				ProductName: "Iron plate",
				Num:         10,
			},
			ProductCost{
				ProductName: "Screw",
				Num:         100,
			},
		},
		NeedDep: "DEP_RESC_STORE",
		Building: &ProductBuilding{
			StoreName: "Furnace",
			StoreAdd:  1000,
		},
		CostIncreaser: ProductCostIncreaser{IncStep: 1, IncFactor: 20},
	}

	ProductMap["Factory store"] = &Product{
		CostTime: 6000,
		CreateCost: []ProductCost{
			ProductCost{
				ProductName: "Iron plate",
				Num:         10,
			},
			ProductCost{
				ProductName: "Screw",
				Num:         100,
			},
		},
		NeedDep: "DEP_RESC_STORE",
		Building: &ProductBuilding{
			StoreName: "Factory",
			StoreAdd:  1000,
		},
		CostIncreaser: ProductCostIncreaser{IncStep: 1, IncFactor: 20},
	}

	ProductMap["Adv Factory store"] = &Product{
		CostTime: 6000,
		CreateCost: []ProductCost{
			ProductCost{
				ProductName: "Iron plate",
				Num:         10,
			},
			ProductCost{
				ProductName: "Screw",
				Num:         100,
			},
		},
		NeedDep: "DEP_RESC_STORE",
		Building: &ProductBuilding{
			StoreName: "Advanced factory",
			StoreAdd:  1000,
		},
		CostIncreaser: ProductCostIncreaser{IncStep: 1, IncFactor: 20},
	}
}
