package game

import (
	"math"
	"terminal_wars/internal/formater"
)

type StoreFile struct {
	Count int `json:",omitempty"`
	Max   int `json:",omitempty"`
}
type Store struct {
	count       int
	max         int
	originalMax int
	product     *Product
}

func NewStore(max int, p *Product) *Store {
	return &Store{
		count:       0,
		max:         max,
		originalMax: max,
		product:     p,
	}
}
func (s *Store) Reset() {
	defStore := NewStore(s.originalMax, s.product)
	s.max = defStore.originalMax
	s.count = defStore.count
	s.originalMax = defStore.originalMax
}

func (s *Store) SaveData() StoreFile {
	return StoreFile{Count: s.count, Max: s.max}
}
func (s *Store) LoadData(f StoreFile) {
	s.count = f.Count
	s.max = f.Max
}

func (s *Store) GetProduct() *Product {
	return s.product
}

func (s *Store) Add(num int) bool {
	s.count += num
	if s.count < 0 {
		s.count = 0
	}
	if s.max != -1 && s.count > s.max {
		s.count = s.max
	}
	return s.count == s.max
}

func (s *Store) GetFreeSpace() int {
	if s.max == -1 {
		return math.MaxInt32
	}
	return s.max - s.count
}

func (s *Store) CheckStore(num int) bool {
	return s.count >= num
}

func (s *Store) GetCount() int {
	return s.count
}

func (s *Store) GetMax() int {
	return s.max
}

func (s *Store) GetInfo() string {
	if s.max == -1 {
		return formater.ValueFormat(s.count)
	}
	valueText := formater.ValueFormat(s.count)
	if s.count == 0 {
		valueText = "[black:white]" + valueText + "[-:black]"
	}
	return valueText + "/" + formater.ValueFormat(s.max)
}

func (s *Store) SetMax(max int) {
	s.max = max
}

func (s *Store) AddMax(add int) {
	s.max += add
}
