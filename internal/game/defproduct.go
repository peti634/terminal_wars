package game

func createMineProduct(costTime int, dep string) *Product {
	return &Product{
		CostTime:   costTime,
		CreateCost: []ProductCost{},
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Mine"],
		},
		NeedDep: dep,
	}
}

func createFurnaceProduct(costTime int, dep string, pc []ProductCost) *Product {
	return &Product{
		CostTime:   costTime,
		CreateCost: pc,
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Furnace"],
		},
		NeedDep: dep,
	}
}

func createFactoryProduct(costTime int, dep string, pc []ProductCost) *Product {
	return &Product{
		CostTime:   costTime,
		CreateCost: pc,
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Factory"],
		},
		NeedDep: dep,
	}
}

func initProduct() {
	////////////////////RESOURCE PRODUCTS///////////////////////
	//////////////////////////////MINE/////////////////////////
	ProductMap["Iron"] = createMineProduct(3000, "DEP_BUILD_MINE")
	ProductMap["Copper"] = createMineProduct(3000, "DEP_BUILD_MINE")
	ProductMap["Coal"] = createMineProduct(3000, "DEP_BUILD_MINE")
	ProductMap["Silicon"] = createMineProduct(8000, "DEP_RESC_SILICON")

	//////////////////////////////FURNACE/////////////////////////
	ProductMap["Iron plate"] = createFurnaceProduct(5000, "DEP_BUILD_FURNACE",
		[]ProductCost{
			ProductCost{
				Num:         2,
				ProductName: "Iron",
			},
			ProductCost{
				Num:         1,
				ProductName: "Coal",
			},
		})
	ProductMap["Copper plate"] = createFurnaceProduct(5000, "DEP_BUILD_FURNACE",
		[]ProductCost{
			ProductCost{
				Num:         2,
				ProductName: "Copper",
			},
			ProductCost{
				Num:         1,
				ProductName: "Coal",
			},
		})

	ProductMap["Gunpowder"] = createFurnaceProduct(5000, "DEP_RESC_DEF",
		[]ProductCost{
			ProductCost{
				Num:         2,
				ProductName: "Coal",
			},
		})
	ProductMap["Silicon plate"] = createFurnaceProduct(11000, "DEP_RESC_SILICON",

		[]ProductCost{
			ProductCost{
				Num:         2,
				ProductName: "Silicon",
			},
			ProductCost{
				Num:         1,
				ProductName: "Coal",
			},
		})

	ProductMap["Iron block"] = createFurnaceProduct(5000, "DEP_RESC_ARMYROBOT",
		[]ProductCost{
			ProductCost{
				Num:         5,
				ProductName: "Iron",
			},
			ProductCost{
				Num:         1,
				ProductName: "Coal",
			},
		})
	//////////////////////////////FACTORY/////////////////////////
	ProductMap["Copper cable"] = createFactoryProduct(4000, "DEP_BUILD_FACTORY",
		[]ProductCost{
			ProductCost{
				Num:         1,
				ProductName: "Copper plate",
			},
		})
	ProductMap["Bulb"] = createFactoryProduct(12000, "DEP_BUILD_FACTORY",
		[]ProductCost{
			ProductCost{
				Num:         1,
				ProductName: "Copper cable",
			},
			ProductCost{
				Num:         1,
				ProductName: "Iron plate",
			},
		})
	ProductMap["Basic lab pack"] = &Product{
		CostTime: 25000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         4,
				ProductName: "Bulb",
			},
			ProductCost{
				Num:         1,
				ProductName: "Copper plate",
			},
			ProductCost{
				Num:         1,
				ProductName: "Iron plate",
			},
		},
		UnlimitStore: true,
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Factory"],
		},
		NeedDep: "DEP_BUILD_FACTORY",
	}

	ProductMap["Screw"] = &Product{
		CostTime: 10000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         1,
				ProductName: "Iron plate",
			},
		},
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Factory"],
		},
		NeedDep: "DEP_RESC_STORE",
	}

	ProductMap["Circuit"] = &Product{
		CostTime: 8000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         1,
				ProductName: "Iron plate",
			},
			ProductCost{
				Num:         3,
				ProductName: "Copper cable",
			},
			ProductCost{
				Num:         2,
				ProductName: "Silicon plate",
			},
		},
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Factory"],
		},
		NeedDep: "DEP_RESC_SILICON",
	}

	ProductMap["Improve lab pack"] = &Product{
		CostTime: 40000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         4,
				ProductName: "Circuit",
			},
			ProductCost{
				Num:         1,
				ProductName: "Basic lab pack",
			},
		},
		UnlimitStore: true,
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Factory"],
		},
		NeedDep: "DEP_RESC_IMPROVLAB",
	}
	//////////////////////////////ADV FACTORY/////////////////////////

	ProductMap["Engine"] = &Product{
		CostTime: 60000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         1,
				ProductName: "Iron plate",
			},
			ProductCost{
				Num:         2,
				ProductName: "Copper cable",
			},
		},
		Producer: &ProductProducerDetails{
			FreeProducer: FreeProducerMap["Advanced factory"],
		},
		NeedDep: "DEP_RESC_ADVFACTORY",
	}
}
