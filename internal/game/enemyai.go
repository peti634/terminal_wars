package game

import (
	"math/rand"

	"terminal_wars/internal/formater"
)

const PLAYER_ATTACKTIME_FROM = 150 * 1000
const PLAYER_ATTACKTIME_RANGE = 1 * 60 * 1000

const AIPOINT_FACTORINC_PERCENT = 3
const AIPOINT_TURRET_PERCENT = 15
const AIPOINT_INCMAXHP_PERCENT = 60

const AIPOINT_FACTORINC_START = 13000
const AIPOINT_AMMO = 150 * 1000
const AIPOINT_WALLREP = 10 * 1000
const AIPOINT_TURRET_START = 2 * 1000 * 1000
const AIPOINT_INCMAXHP_START = 20 * 1000 * 1000

const AIFACTOR_START = 500
const AIFACTOR_INC_PERCENT = 1
const AICHECK_DROPTOOMUCHTIME = 40 //drop if this goal required too much time in sec

const AISTART_TURRET = 1
const AISTART_WALL = 100
const AISTART_TURRET_GOAL = 5
const AINEED_AMMO = 100

const AIARMY_ATTACKTIME_FROM = 2 * 60 * 1000
const AIARMY_ATTACKTIME_RANGE = 3 * 60 * 1000 //time: random between FROM and FROM + RANGE
const AIARMY_RELAXTIME_FROM = 2 * 60 * 1000
const AIARMY_RELAXTIME_RANGE = 4 * 60 * 1000 //time: random between FROM and FROM + RANGE

const AIARMY_FIRST_ATTACK = 15 * 60 * 1000
const AISLEEP_TIME = 20 * 60 * 1000

type EnemyAIFile struct {
	Point       int
	PointFactor int

	SleepTimer int
	RelaxTimer int //after every attack have to wait it

	PreArmyPoint int

	NeedUpgDefenseCounter int

	CurrentIncFactorPoint int //current increase factor point
	CurrentWallMaxHpPoint int //current wall max hp point
	CurrentTurretPoint    int //current turret point

	//army counters
	ArmyCounterRobot int
	ArmyCounterTank  int
}

type EnemyAI struct {
	point       int
	pointFactor int
	goal        rune
	goalArmy    rune
	sleepTimer  int
	relaxTimer  int //after every attack have to wait it

	selfAttacker   *Attacker
	playerAttacker *Attacker

	preArmyPoint int

	//calulated from prev player attacked
	//if it was too strig, need more def
	needUpgDefenseCounter int

	currentIncFactorPoint int //current increase factor point
	currentWallMaxHpPoint int //current wall max hp point
	currentTurretPoint    int //current turret point

	//army counters
	armyCounterRobot int
	armyCounterTank  int
}

/*
GOAL LIST:
' '- nothing
'I'- increase point factor
'T'- build turret
'H'- increse wall max hp
'D'- it will be turret or max hp
'R'- repair wall

'A'- army build


*/

func NewEnemyAI(selfAt *Attacker, playerAt *Attacker) *EnemyAI {
	enemy := &EnemyAI{
		sleepTimer:            AISLEEP_TIME,
		pointFactor:           AIFACTOR_START,
		goal:                  ' ',
		goalArmy:              ' ',
		currentIncFactorPoint: AIPOINT_FACTORINC_START,
		currentTurretPoint:    AIPOINT_TURRET_START,
		currentWallMaxHpPoint: AIPOINT_INCMAXHP_START,
		selfAttacker:          selfAt,
		playerAttacker:        playerAt,
	}

	enemy.playerAttacker.turret.ammo.Add(AINEED_AMMO)
	enemy.playerAttacker.turret.count.Add(AISTART_TURRET)
	enemy.playerAttacker.wall.Add(AISTART_WALL)
	return enemy
}

func (ai *EnemyAI) SaveData() EnemyAIFile {
	return EnemyAIFile{
		Point:       ai.point,
		PointFactor: ai.pointFactor,

		SleepTimer: ai.sleepTimer,
		RelaxTimer: ai.relaxTimer, //after every attack have to wait it

		PreArmyPoint: ai.preArmyPoint,

		NeedUpgDefenseCounter: ai.needUpgDefenseCounter,

		CurrentIncFactorPoint: ai.currentIncFactorPoint, //current increase factor point
		CurrentWallMaxHpPoint: ai.currentWallMaxHpPoint, //current wall max hp point
		CurrentTurretPoint:    ai.currentTurretPoint,    //current turret point

		//army counters
		ArmyCounterRobot: ai.armyCounterRobot,
		ArmyCounterTank:  ai.armyCounterTank,
	}
}

func (ai *EnemyAI) LoadData(f EnemyAIFile) {
	ai.point = f.Point
	ai.pointFactor = f.PointFactor

	ai.sleepTimer = f.SleepTimer
	ai.relaxTimer = f.RelaxTimer

	ai.preArmyPoint = f.PreArmyPoint

	ai.needUpgDefenseCounter = f.NeedUpgDefenseCounter

	ai.currentIncFactorPoint = f.CurrentIncFactorPoint
	ai.currentWallMaxHpPoint = f.CurrentWallMaxHpPoint
	ai.currentTurretPoint = f.CurrentTurretPoint

	ai.armyCounterRobot = f.ArmyCounterRobot
	ai.armyCounterTank = f.ArmyCounterTank
}

func RandYesOrNoPercent(yes int) bool {
	return rand.Intn(10000) < yes
}

func incPercent(v *int, p int) {
	*v += (int(*v) * p) / 100
}

//if it spend too much time, then drop this goal
func (ai *EnemyAI) CheckTooHighPoint(point int) {
	if (point-ai.point)/ai.pointFactor >= AICHECK_DROPTOOMUCHTIME*10 {
		ai.goal = ' '
		ai.goalArmy = ' '
	}
}
func (ai *EnemyAI) buildArmy() {
	switch ai.goalArmy {
	case ' ':
		if RandYesOrNoPercent(7000) {
			ai.goalArmy = 'R'
		} else {
			ai.goalArmy = 'T'
		}
	case 'R':
		ai.CheckTooHighPoint(ProductMap["Robot warrior"].AI.PointCost)
		if ai.point >= ProductMap["Robot warrior"].AI.PointCost {
			ai.point -= ProductMap["Robot warrior"].AI.PointCost
			ai.armyCounterRobot++
			ai.goal = ' '
			ai.goalArmy = ' '
		}
	case 'T':
		ai.CheckTooHighPoint(ProductMap["Tank"].AI.PointCost)
		if ai.point >= ProductMap["Tank"].AI.PointCost {
			ai.point -= ProductMap["Tank"].AI.PointCost
			ai.armyCounterTank++
			ai.goal = ' '
			ai.goalArmy = ' '
		}
	}
}
func (ai *EnemyAI) GetArmyPoint() int {
	var ret int
	ret += ProductMap["Robot warrior"].AI.PointCost * ai.armyCounterRobot
	ret += ProductMap["Tank"].AI.PointCost * ai.armyCounterTank

	return ret
}

func (ai *EnemyAI) Calc() {
	if ai.sleepTimer > 0 {
		ai.sleepTimer -= 100
		return
	}

	selfTurret := &ai.playerAttacker.turret
	selfWall := ai.playerAttacker.wall    //it is a store
	if selfTurret.ammo.GetCount() < 100 { //Check that need ammo
		selfTurret.ammo.Add(10)
		ai.point -= AIPOINT_AMMO * 10
	}

	//fmt.Println(formater.ValueFormat(ai.pointFactor) + "_" + strconv.Itoa(selfTurret.count.GetCount()))

	if ai.playerAttacker.timer == 0 { //check attack
		wallHPPercent := (selfWall.GetCount() * 100) / selfWall.GetMax()
		if wallHPPercent <= 50 {
			//100 / 50% = 2
			//100 / 10% = 10
			//100 / 1 = 100

			if wallHPPercent == 0 {
				ai.needUpgDefenseCounter = 200
			} else {
				ai.needUpgDefenseCounter = 100 / wallHPPercent
			}
		}

	} else {
		//only no attacking, yes, it is easier
		ai.point += ai.pointFactor

		if ai.goal == ' ' {
			optList := []rune{'I', 'A'} //increase and army are default
			if ai.needUpgDefenseCounter > 0 ||
				selfTurret.count.GetCount() < AISTART_TURRET_GOAL {
				optList = append(optList, 'D') //add turret, and wall max hp
			}
			if selfWall.GetCount() != selfWall.GetMax() {
				optList = append(optList, 'R') //add wall repair
			}

			//decision what we will do it
			r := rand.Intn(len(optList))

			optRune := optList[r]

			//rarely
			if optRune == 'I' && RandYesOrNoPercent(100) { //1%
				optRune = 'D'
			}

			if optRune == 'D' {
				if RandYesOrNoPercent(1000) {
					optRune = 'H' //10%
				} else {
					optRune = 'T' //90%
				}
				ai.needUpgDefenseCounter--
			}

			ai.goal = optRune
		}

		switch ai.goal {
		case 'R':
			ai.CheckTooHighPoint(AIPOINT_WALLREP)
			if ai.point >= AIPOINT_WALLREP {
				ai.point -= AIPOINT_WALLREP
				selfWall.Add(1)
				ai.goal = ' '
			}
		case 'H':
			ai.CheckTooHighPoint(ai.currentWallMaxHpPoint)
			if ai.point >= ai.currentWallMaxHpPoint {
				ai.point -= ai.currentWallMaxHpPoint
				incPercent(&ai.currentWallMaxHpPoint, AIPOINT_INCMAXHP_PERCENT)
				selfWall.AddMax(1000)
				ai.goal = ' '
			}
		case 'T':
			ai.CheckTooHighPoint(ai.currentTurretPoint)
			if ai.point >= ai.currentTurretPoint {
				ai.point -= ai.currentTurretPoint
				incPercent(&ai.currentTurretPoint, AIPOINT_TURRET_PERCENT)
				selfTurret.count.Add(1)
				//info.Info("Build turret")
				ai.goal = ' '
			}
		case 'I':
			if ai.point >= ai.currentIncFactorPoint {
				ai.point -= ai.currentIncFactorPoint
				incPercent(&ai.currentIncFactorPoint, AIPOINT_FACTORINC_PERCENT)
				incPercent(&ai.pointFactor, AIFACTOR_INC_PERCENT)
				ai.goal = ' '
			}
		case 'A':
			ai.buildArmy()
		}

		if ai.selfAttacker.timer == -1 && ai.relaxTimer > 0 {
			ai.relaxTimer -= 100
		}

		//send attack?
		if RandYesOrNoPercent(10) && ai.relaxTimer <= 0 && ai.selfAttacker.timer == -1 {
			attackTime := rand.Intn(AIARMY_ATTACKTIME_RANGE) + AIARMY_ATTACKTIME_FROM
			if ai.preArmyPoint == 0 {
				ai.selfAttacker.AddAttacker(AIARMY_FIRST_ATTACK,
					[]AttackerArmyGroup{
						AttackerArmyGroup{Army: ProductMap["Robot warrior"], Count: 5},
					})
				ai.preArmyPoint = ProductMap["Robot warrior"].AI.PointCost * 5
				ai.relaxTimer = rand.Intn(AIARMY_RELAXTIME_FROM) + AIARMY_RELAXTIME_RANGE
			}

			//calculate current all army point
			curretArmyPoint := ai.GetArmyPoint()
			//more then 40%
			if curretArmyPoint >= (ai.preArmyPoint*140)/100 {
				var army = make([]AttackerArmyGroup, 0)
				if ai.armyCounterTank > 0 {
					army = append(army, AttackerArmyGroup{
						Army:  ProductMap["Tank"],
						Count: ai.armyCounterTank})
					ai.armyCounterTank = 0
				}
				if ai.armyCounterRobot > 0 {
					army = append(army, AttackerArmyGroup{
						Army:  ProductMap["Robot warrior"],
						Count: ai.armyCounterRobot})
					ai.armyCounterRobot = 0
				}
				ai.selfAttacker.AddAttacker(attackTime, army)
				ai.preArmyPoint = curretArmyPoint
				ai.relaxTimer = rand.Intn(AIARMY_RELAXTIME_FROM) + AIARMY_RELAXTIME_RANGE
			}

		}

	}

}

func (ai *EnemyAI) GetDebugInfo() string {
	var ret string
	ret += "AI DEBUG" + "\r\n"
	if ai.sleepTimer == 0 {
		ret += formater.ValueFormat(ai.point) + "+" + formater.ValueFormat(ai.pointFactor) +
			" G: " + string(ai.goal) + " GA: " + string(ai.goalArmy) + "\r\n"
		ret += "relaxtimer: " + formater.TimeFormat(ai.relaxTimer) + "\r\n"
		ret += "armyp: " + formater.ValueFormat(ai.GetArmyPoint()) +
			" pre: " + formater.ValueFormat(ai.preArmyPoint) +
			" need: " + formater.ValueFormat((ai.preArmyPoint*140)/100) + "\r\n"

		ret += "current: " +
			"I" + formater.ValueFormat(ai.currentIncFactorPoint) +
			"T" + formater.ValueFormat(ai.currentTurretPoint) +
			"H" + formater.ValueFormat(ai.currentWallMaxHpPoint) +
			"\r\n"
		ret += "Army: R" + formater.ValueFormat(ai.armyCounterRobot) +
			"T" + formater.ValueFormat(ai.armyCounterTank) +
			"\r\n"

	} else {
		ret += "sleep: " + formater.TimeFormat(ai.sleepTimer)
	}

	return ret
}
