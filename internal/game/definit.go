package game

import "sort"

const DEF_STOREMAX = 4000
const DEF_WALLMAX = 1000

var (
	ProductMap      map[string]*Product
	ProducerMap     map[string]*Producer
	StoreMap        map[string]*Store
	FreeProducerMap map[string]*Producer

	ProductKeys []string
)

func InitDefProducer() {

	FreeProducerMap = map[string]*Producer{}
	FreeProducerMap["Mine"] = NewProducer(nil, nil)
	FreeProducerMap["Furnace"] = NewProducer(nil, nil)
	FreeProducerMap["Factory"] = NewProducer(nil, nil)
	FreeProducerMap["Advanced factory"] = NewProducer(nil, nil)

	FreeProducerMap["Defense builder"] = NewProducer(nil, nil)
	FreeProducerMap["Army factory"] = NewProducer(nil, nil)

	StoreMap = make(map[string]*Store)
	ProductMap = make(map[string]*Product)
	ProducerMap = make(map[string]*Producer)

	initProduct()
	initResearch()
	initBuildings()
	initArmydef()

	for i, p := range ProductMap {
		p.Name = i

		if p.Producer != nil {
			StoreMap[i] = NewStore(IfI(p.UnlimitStore, -1, DEF_STOREMAX), p)
			ProducerMap[i] = NewProducer(StoreMap[i], p.Producer.FreeProducer)
		}

	}

	for _, p := range ProductMap {
		//CreateCost cache resolve
		for ccindex, cc := range p.CreateCost {
			p.CreateCost[ccindex].CachedStore = StoreMap[cc.ProductName]
		}

		p.CostIncreaser.StoreOriginalCost(p)

	}

	StoreMap["Wall repair"].SetMax(DEF_WALLMAX)
	StoreMap["Enemy Wall repair"] = NewStore(DEF_WALLMAX, ProductMap["Wall repair"])
	StoreMap["Enemy Turret"] = NewStore(-1, ProductMap["Turret"])
	StoreMap["Enemy Turret ammo"] = NewStore(-1, ProductMap["Turret ammo"])

	ProductKeys = make([]string, len(ProductMap))
	var i int
	for key := range ProductMap {
		ProductKeys[i] = key
		i++
	}
	sort.Strings(ProductKeys)

}

func IfI(b bool, v1 int, v2 int) int {
	if b {
		return v1
	}
	return v2
}

func SearchProductNameByBuildingName(bname string) []string {
	ret := make([]string, 0)

	//first, we have to find "FreeProducer" to building name
	bProduct := ProductMap[bname]
	if bProduct == nil {
		panic("Not found product: " + bname)
	}
	freeProducer := bProduct.Building.ProducerAdd

	//now we will collect all product where find this freeProducer
	for i, v := range ProductMap {
		if v.Producer != nil && v.Producer.FreeProducer == freeProducer {
			ret = append(ret, i)
		}
	}
	return ret
}
