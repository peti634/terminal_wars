package game

import "strconv"

func createResearchB(costT int, numBasic int, depresv string, needdep string) *Product {
	return &Product{
		CostTime: costT,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         numBasic,
				ProductName: "Basic lab pack",
			},
		},
		Research: &ProductResearch{DepResolver: depresv},
		NeedDep:  needdep,
	}
}

func createResearchBI(costT int, numBasic int, numImprove int, depresv string, needdep string) *Product {
	return &Product{
		CostTime: costT,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         numBasic,
				ProductName: "Basic lab pack",
			},
			ProductCost{
				Num:         numImprove,
				ProductName: "Improve lab pack",
			},
		},
		Research: &ProductResearch{DepResolver: depresv},
		NeedDep:  needdep,
	}
}

func createReResearchI(costT int, numImprove int, needdep string) *Product {
	return &Product{
		CostTime: costT * 1000,
		CreateCost: []ProductCost{
			ProductCost{
				Num:         numImprove,
				ProductName: "Improve lab pack",
			},
		},
		Research: &ProductResearch{CanReResearch: true},
		NeedDep:  needdep,

		CostIncreaser: ProductCostIncreaser{IncStep: 1, IncFactor: 30},
	}

}

func setFasterResearch(p *Product, factor int, productName string) {
	p.Research.FasterProducerName = productName
	p.Research.Factor = factor
	p.Research.ShowText = "It will increase the " + productName + " product speed.\r\n" +
		"Increase speed: " + strconv.Itoa(factor) + "%"
}

func setCheaperResearch(p *Product, factor int, productName string) {
	p.Research.CheaperProductName = productName
	p.Research.Factor = factor
	p.Research.ShowText = "It will reduction the " + productName + " build cost.\r\n" +
		"Cost redcution: " + strconv.Itoa(factor) + "%"
}

func initResearch() {
	ProductMap["Info menu resch"] = createResearchB(10000, 10, "DEP_RESC_INFO", "DEP_BUILD_FACTORY")
	ProductMap["Store resch"] = createResearchB(40000, 40, "DEP_RESC_STORE", "DEP_BUILD_FACTORY")
	ProductMap["Silicon mine resch"] = createResearchB(60000, 90, "DEP_RESC_SILICON", "DEP_RESC_STORE")
	ProductMap["Multiple build resch"] = createResearchB(15000, 10, "DEP_RESC_MULTIPLE", "DEP_RESC_STORE")

	ProductMap["Improve lab pack resch"] = createResearchB(120000, 70, "DEP_RESC_IMPROVLAB", "DEP_RESC_SILICON")
	ProductMap["Improving resch"] = createResearchBI(90000, 100, 50, "DEP_RESC_IMPROV", "DEP_RESC_IMPROVLAB")
	ProductMap["Small upgrade resch"] = createResearchBI(15000, 500, 300, "DEP_RESC_SMUPG", "DEP_RESC_IMPROV")

	ProductMap["Defense resch"] = createResearchB(15000, 10, "DEP_RESC_DEF", "DEP_BUILD_FACTORY")
	ProductMap["Wall resch"] = createResearchB(15000, 20, "DEP_RESC_DEFWALL", "DEP_RESC_DEF")
	ProductMap["Turret resch"] = createResearchB(15000, 30, "DEP_RESC_DEFTURR", "DEP_RESC_DEFWALL")
	ProductMap["Turret ammo resch"] = createResearchB(15000, 40, "DEP_RESC_DEFFAMMO", "DEP_RESC_DEFTURR")
	ProductMap["Inc. Wall maxhp resch"] = createReResearchI(30, 300, "DEP_RESC_DEFFAMMO")

	ProductMap["Army resch"] = createResearchB(60000, 100, "DEP_RESC_ARMY", "DEP_RESC_SILICON")
	ProductMap["Robot resch"] = createResearchB(60000, 200, "DEP_RESC_ARMYROBOT", "DEP_RESC_ARMY")
	ProductMap["Adv factory resch"] = createResearchB(60000, 300, "DEP_RESC_ADVFACTORY", "DEP_RESC_ARMYROBOT")
	ProductMap["Tank resch"] = createResearchB(60000, 400, "DEP_RESC_TANK", "DEP_RESC_ADVFACTORY")

	ProductMap["Fast research resch"] = createReResearchI(30, 50, "")
	ProductMap["Fast build resch"] = createReResearchI(30, 50, "")
	ProductMap["Fast mine resch"] = createReResearchI(20, 50, "")
	ProductMap["Fast mine resch"].CostIncreaser.IncFactor = 20
	ProductMap["Fast furnace resch"] = createReResearchI(20, 50, "")
	ProductMap["Fast factory resch"] = createReResearchI(20, 50, "")
	ProductMap["Fast adv factory resch"] = createReResearchI(20, 50, "DEP_RESC_ADVFACTORY")
	ProductMap["Fast defense resch"] = createReResearchI(20, 50, "DEP_RESC_DEF")
	ProductMap["Fast army resch"] = createReResearchI(20, 50, "DEP_RESC_ARMY")

	ProductMap["Cheaper mine resch"] = createReResearchI(20, 50, "")
	ProductMap["Cheaper mine resch"].CostIncreaser.IncFactor = 20
	ProductMap["Cheaper furnace resch"] = createReResearchI(20, 50, "")
	ProductMap["Cheaper factory resch"] = createReResearchI(20, 50, "")
	ProductMap["Cheaper adv factory resch"] = createReResearchI(20, 50, "DEP_RESC_ADVFACTORY")
	ProductMap["Cheaper store resch"] = createReResearchI(20, 50, "")
	ProductMap["Cheaper defense resch"] = createReResearchI(20, 50, "DEP_RESC_DEF")
	ProductMap["Cheaper army resch"] = createReResearchI(20, 50, "DEP_RESC_ARMY")

	setFasterResearch(ProductMap["Fast research resch"], 400, "Research")
	setFasterResearch(ProductMap["Fast build resch"], 400, "Build")
	setFasterResearch(ProductMap["Fast mine resch"], 12, "Mine")
	setFasterResearch(ProductMap["Fast furnace resch"], 12, "Furnace")
	setFasterResearch(ProductMap["Fast factory resch"], 12, "Factory")
	setFasterResearch(ProductMap["Fast adv factory resch"], 12, "Advanced factory")
	setFasterResearch(ProductMap["Fast defense resch"], 12, "Defense builder")
	setFasterResearch(ProductMap["Fast army resch"], 12, "Army factory")

	setCheaperResearch(ProductMap["Cheaper mine resch"], 12, "Mine")
	setCheaperResearch(ProductMap["Cheaper furnace resch"], 12, "Furnace")
	setCheaperResearch(ProductMap["Cheaper factory resch"], 12, "Factory")
	setCheaperResearch(ProductMap["Cheaper adv factory resch"], 12, "Advanced factory")
	setCheaperResearch(ProductMap["Cheaper store resch"], 60, "Store")
	setCheaperResearch(ProductMap["Cheaper defense resch"], 12, "Defense builder")
	setCheaperResearch(ProductMap["Cheaper army resch"], 12, "Army factory")

	ProductMap["Fast research resch"].Research.FasterProducerName = ""
	ProductMap["Fast build resch"].Research.FasterProducerName = ""
	ProductMap["Cheaper store resch"].Research.CheaperProductName = ""

	ProductMap["Inc. Wall maxhp resch"].Research.Factor = 1000
	ProductMap["Inc. Wall maxhp resch"].Research.ShowText = "" +
		"It will increase the wall max health.\r\n" +
		"Increase: " + strconv.Itoa(ProductMap["Inc. Wall maxhp resch"].Research.Factor)

	//////////////////SPECIAL SET///////////////////

	ProductMap["Fast build resch"].Research.FinishedFunc = func(p *Product) {
		Builder.AddFactor(p.Research.Factor)
	}
	ProductMap["Fast research resch"].Research.FinishedFunc = func(p *Product) {
		Researcher.AddFactor(p.Research.Factor)
	}
	ProductMap["Cheaper store resch"].Research.FinishedFunc = func(p *Product) {
		ProductMap["Mine store"].CostIncreaser.AddDecValue(p.Research.Factor)
		ProductMap["Furnace store"].CostIncreaser.AddDecValue(p.Research.Factor)
		ProductMap["Factory store"].CostIncreaser.AddDecValue(p.Research.Factor)
		ProductMap["Adv Factory store"].CostIncreaser.AddDecValue(p.Research.Factor)
	}

	ProductMap["Inc. Wall maxhp resch"].Research.FinishedFunc = func(p *Product) {
		StoreMap["Wall repair"].AddMax(p.Research.Factor)
	}

	ProductMap["Small upgrade resch"].Research.ShowText = "" +
		"It will be improve and cheaper all buildings with 20% and increase wall max hp with 10000"
	ProductMap["Small upgrade resch"].Research.FinishedFunc = func(p *Product) {

		improve := func(improveproductname string) {
			producerList := SearchProductNameByBuildingName(improveproductname)
			for _, pname := range producerList {
				producer := ProducerMap[pname]
				producer.AddFactor(20)
			}

			product := ProductMap[improveproductname]
			product.CostIncreaser.AddDecValue(20)

		}
		improve("Mine")
		improve("Furnace")
		improve("Factory")
		improve("Advanced factory")
		improve("Defense builder")
		improve("Army factory")

		ProductMap["Mine store"].CostIncreaser.AddDecValue(20)
		ProductMap["Furnace store"].CostIncreaser.AddDecValue(20)
		ProductMap["Factory store"].CostIncreaser.AddDecValue(20)
		ProductMap["Adv Factory store"].CostIncreaser.AddDecValue(20)

		Builder.AddFactor(20)
		Researcher.AddFactor(20)
		StoreMap["Wall repair"].AddMax(10000)

	}
}
