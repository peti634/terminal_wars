package game

import "terminal_wars/internal/info"

const (
	DEPC_MINE10 = iota
)

type DepGift struct {
	ProductName string
	Value       int
}
type DepItem struct {
	checked bool
	gift    []DepGift
}

var (
	depMap        = make(map[string]*DepItem)
	changeDepFunc func(string, []DepGift)
)

func init() {
	depMap["DEP_BUILD_MINE"] = &DepItem{}       //Build mine for its job & Furnace
	depMap["DEP_BUILD_FURNACE"] = &DepItem{}    //Build furnace for its job & Factory
	depMap["DEP_BUILD_FACTORY"] = &DepItem{}    //Build factory for its job & Research
	depMap["DEP_BUILD_ADVFACTORY"] = &DepItem{} //Build advenced factory for its job
	depMap["DEP_BUILD_DEF"] = &DepItem{}        //Build defense for its job
	depMap["DEP_BUILD_ARMY"] = &DepItem{}       //Build army for its job

	depMap["DEP_RESC_STORE"] = &DepItem{}     //Store research
	depMap["DEP_RESC_MULTIPLE"] = &DepItem{}  //Multiple build research
	depMap["DEP_RESC_SILICON"] = &DepItem{}   //Silicon mine research
	depMap["DEP_RESC_IMPROVLAB"] = &DepItem{} //Improve lab pack research
	depMap["DEP_RESC_IMPROV"] = &DepItem{}    //Improve researchó
	depMap["DEP_RESC_SMUPG"] = &DepItem{}     //Small upgrade research

	depMap["DEP_RESC_DEF"] = &DepItem{}      //Defense research
	depMap["DEP_RESC_DEFWALL"] = &DepItem{}  //Wall research
	depMap["DEP_RESC_DEFTURR"] = &DepItem{}  //Turret research
	depMap["DEP_RESC_DEFFAMMO"] = &DepItem{} //Turret ammo research

	depMap["DEP_RESC_ARMY"] = &DepItem{}       //Defense research
	depMap["DEP_RESC_ARMYROBOT"] = &DepItem{}  //Wall research
	depMap["DEP_RESC_ADVFACTORY"] = &DepItem{} //Turret research
	depMap["DEP_RESC_TANK"] = &DepItem{}       //Turret ammo research

	depMap["DEP_RESC_INFO"] = &DepItem{} //Info menu research

	depMap["DEP_HIDDEN"] = &DepItem{} //It will never showing

	//set gifts
	depMap["DEP_BUILD_MINE"].gift = []DepGift{
		DepGift{ProductName: "Iron", Value: 100},
		DepGift{ProductName: "Copper", Value: 100},
	}
	depMap["DEP_BUILD_FURNACE"].gift = []DepGift{
		DepGift{ProductName: "Iron", Value: 200},
		DepGift{ProductName: "Copper", Value: 100},
	}
	depMap["DEP_BUILD_FACTORY"].gift = []DepGift{
		DepGift{ProductName: "Iron", Value: 400},
		DepGift{ProductName: "Copper", Value: 300},
		DepGift{ProductName: "Coal", Value: 100},
	}
	depMap["DEP_RESC_STORE"].gift = []DepGift{
		DepGift{ProductName: "Iron plate", Value: 1000},
		DepGift{ProductName: "Copper plate", Value: 1000},
	}
	depMap["DEP_RESC_DEFWALL"].gift = []DepGift{
		DepGift{ProductName: "Wall repair", Value: 100},
	}
	depMap["DEP_RESC_DEFTURR"].gift = []DepGift{
		DepGift{ProductName: "Turret", Value: 2},
	}
	depMap["DEP_RESC_DEFFAMMO"].gift = []DepGift{
		DepGift{ProductName: "Turret ammo", Value: 5},
	}
}

func DepReset() {
	for _, item := range depMap {
		item.checked = false
	}
}

func DepSaveData() map[string]bool {
	ret := map[string]bool{}

	for name, item := range depMap {
		ret[name] = item.checked
	}
	return ret
}

func DepLoadData(depmapparam map[string]bool) {
	for name, item := range depmapparam {
		if depMap[name] != nil {
			depMap[name].checked = item
		}
	}
}

func CheckDep(dep string) bool {

	if dep == "" {
		return true
	}
	if depMap[dep] == nil {
		info.Info("ERROR: Dep not found: " + dep)
		return false
	}
	if depMap[dep].Check() == false {
		return false
	}

	return true
}

func DepResolve(depName string) {
	if depMap[depName] == nil {
		info.Info("ERROR: Dep resolve not found: " + depName)
		return
	}
	if depMap[depName].checked == false {
		depMap[depName].checked = true
		changeDepFunc(depName, depMap[depName].gift)
	}
}

func DepSetChangeDepFunc(f func(string, []DepGift)) {
	changeDepFunc = f
}

func (d *DepItem) Check() bool {
	return d.checked
}
