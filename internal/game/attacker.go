package game

import (
	"sort"
	"strconv"

	"terminal_wars/internal/formater"
)

type AttackerBase struct {
	Health    int
	MaxHealth int
}

type AttackerArmyGroup struct {
	Army  *Product
	Count int
}
type AttackerTurret struct {
	count     *Store
	ammo      *Store
	ammoPiece int
}

type AttackerFile struct {

	//armyGroup
	Army map[string]int

	TargetUnitHealt int
	Timer           int

	//base
	BaseHealth int
	BaseMax    int
	//turret
	AmmoPiece int

	DamagaPiece int
}

type Attacker struct {
	armyGroup       []AttackerArmyGroup
	targetUnitHealt int
	timer           int
	wall            *Store
	base            AttackerBase
	turret          AttackerTurret
	damagaPiece     int
	loseBaseFunc    func(self *Attacker)
}

func newAttacker(wall *Store, turret *Store, turretammo *Store) *Attacker {
	return &Attacker{
		timer:  -1,
		base:   AttackerBase{MaxHealth: 1000, Health: 1000},
		wall:   wall,
		turret: AttackerTurret{count: turret, ammo: turretammo},
	}
}

func (a *Attacker) Reset() {
	defState := newAttacker(nil, nil, nil)

	a.armyGroup = defState.armyGroup
	a.targetUnitHealt = defState.targetUnitHealt
	a.timer = defState.timer
	a.base = defState.base
	a.turret.ammoPiece = defState.turret.ammoPiece
	a.damagaPiece = defState.damagaPiece
}

func (a *Attacker) SaveData() AttackerFile {
	ret := AttackerFile{
		Army:            map[string]int{},
		TargetUnitHealt: a.targetUnitHealt,
		Timer:           a.timer,

		//base
		BaseHealth: a.base.Health,
		BaseMax:    a.base.MaxHealth,
		//turret
		AmmoPiece: a.turret.ammoPiece,

		DamagaPiece: a.damagaPiece,
	}

	for _, i := range a.armyGroup {
		ret.Army[i.Army.Name] = i.Count
	}
	return ret
}

func (a *Attacker) LoadData(f AttackerFile) {
	a.targetUnitHealt = f.TargetUnitHealt
	a.base.Health = f.BaseHealth
	a.base.MaxHealth = f.BaseMax
	a.turret.ammoPiece = f.AmmoPiece
	a.damagaPiece = f.DamagaPiece

	army := []AttackerArmyGroup{}
	for name, item := range f.Army {
		army = append(army,
			AttackerArmyGroup{
				Army:  ProductMap[name],
				Count: item,
			})
	}
	a.AddAttacker(f.Timer, army)
}

func (a *Attacker) getArmyDamage() int {
	ret := 0
	for _, ag := range a.armyGroup {
		ret += ag.Army.Army.Damage * ag.Count
	}
	ret += a.damagaPiece
	a.damagaPiece = ret % 100
	return ret / 100
}

func (a *Attacker) getTurretShot() int {
	ammoShotCount := (a.turret.ammo.GetCount() * 100) + a.turret.ammoPiece

	if a.turret.count.GetCount() < ammoShotCount {
		ammoShotCount = a.turret.count.GetCount()
	}
	a.takeAmmo(ammoShotCount)
	return ammoShotCount
}
func (a *Attacker) takeAmmo(ammoShotCount int) {
	if a.turret.ammoPiece > 0 {
		save := ammoShotCount
		ammoShotCount -= a.turret.ammoPiece
		a.turret.ammoPiece -= save
	}
	if ammoShotCount > 0 {
		//we need to minimum 1 ammo, because need the ammo piece
		ammoNum := (ammoShotCount / 100) + 1
		a.turret.ammo.Add(-ammoNum)
		a.turret.ammoPiece = 100 - (ammoShotCount % 100)
	}
}

func (a *Attacker) Calc() {
	if a.timer > 0 {
		a.timer -= 100
		if a.timer < 0 {
			a.timer = 0
		}
	} else if a.timer == 0 { //under a attack

		//army attack to base
		allDamage := a.getArmyDamage()

		if a.wall.GetCount() > 0 {
			a.wall.Add(-allDamage)
		} else if a.base.Health > 0 {
			a.base.Health -= allDamage
			if a.base.Health <= 0 {
				a.base.Health = 0
				a.loseBaseFunc(a)
			}
		} else {
			// TODO defeat
		}
		//turret attack to army

		turretAllShot := a.getTurretShot()
		turretDamage := 5
		for turretAllShot > 0 {

			if a.targetUnitHealt == 0 {
				a.targetUnitHealt = a.armyGroup[0].Army.Army.Health
			}
			if a.targetUnitHealt > turretAllShot*turretDamage {
				a.targetUnitHealt -= turretAllShot * turretDamage

				turretAllShot = 0
			} else { //target dead

				turretAllShot -= a.targetUnitHealt
				a.targetUnitHealt = 0
				if a.armyGroup[0].Count > 1 {
					a.armyGroup[0].Count--
				} else {
					a.armyGroup = a.armyGroup[1:]
					if len(a.armyGroup) == 0 {
						a.timer = -1
						break
					}
				}
			}
		}
	}
}

func (a *Attacker) AddAttacker(time int, army []AttackerArmyGroup) {
	if a.timer == -1 {
		a.timer = time

		sort.Slice(army, func(i, j int) bool {
			return army[i].Army.Army.Priority < army[j].Army.Army.Priority
		})

		a.armyGroup = army
	}

}
func (a *Attacker) GetAttackTimeInfo(title string) string {
	if a.timer == -1 {
		return ""
	}
	if a.timer == 0 {
		return title + "Under attack..."
	}
	return title + formater.TimeFormat(a.timer)
}

func (a *Attacker) GetWallInfo() string {
	return a.wall.GetInfo()
}

func (a *Attacker) GetBaseInfo() string {
	return strconv.Itoa(int(a.base.Health)) + "/" + strconv.Itoa(int(a.base.MaxHealth))
}

func (a *Attacker) GetTurretNumInfo() string {
	return strconv.Itoa(int(a.turret.count.GetCount()))
}
func (a *Attacker) GetTurretAmmoInfo() string {
	return strconv.Itoa(int(a.turret.ammo.GetCount())) + " (" + strconv.Itoa(int(a.turret.ammoPiece)) + "%)"
}

func (a *Attacker) GetTargetInfo() string {
	return strconv.Itoa(int(a.targetUnitHealt))
}

func (a *Attacker) SetLoseBaseFunc(f func(self *Attacker)) {
	a.loseBaseFunc = f
}
func (a *Attacker) AttackingProgressing() bool {
	return a.timer >= 0
}
func (a *Attacker) UnderAttack() bool {
	return a.timer == 0
}
func (a *Attacker) GetArmyInfo() string {
	ret := ""
	for _, e := range a.armyGroup {

		ret += string(e.Army.Name[0]) + "(" + strconv.Itoa(int(e.Count)) + ") "
	}
	return ret
}
