package game

import (
	"math/rand"
	"time"
)

var (
	GameTime int

	Builder    *Creator
	Researcher *Creator

	Defense   *Attacker
	Offensive *Attacker

	Enemy *EnemyAI
)

func AddToCreator(p *Product) {
	if p.Building != nil {
		Builder.BuyAndAddQueue(p)
	}
	if p.Research != nil {
		Researcher.BuyAndAddQueue(p)
	}
}

func GameInit() {
	rand.Seed(time.Now().UnixNano())
	InitDefProducer()

	Builder = NewCreator()
	Researcher = NewCreator()

	Defense = newAttacker(
		StoreMap["Wall repair"],
		StoreMap["Turret"],
		StoreMap["Turret ammo"],
	)
	Offensive = newAttacker(
		StoreMap["Enemy Wall repair"],
		StoreMap["Enemy Turret"],
		StoreMap["Enemy Turret ammo"],
	)
	Enemy = NewEnemyAI(Defense, Offensive)
}

func GameResetStartState() {

	StoreMap["Iron"].Add(300)
	StoreMap["Copper"].Add(300)
}

func GameStateReset() {
	GameTime = 0

	DepReset()

	for _, item := range FreeProducerMap {
		item.Reset()
	}

	for _, item := range ProductMap {
		item.Reset()
	}
	for _, item := range ProducerMap {
		item.Reset()
	}

	for _, item := range StoreMap {
		item.Reset()
	}

	Builder.Reset()
	Researcher.Reset()

	Defense.Reset()
	Offensive.Reset()
	Enemy = NewEnemyAI(Defense, Offensive)

	GameResetStartState()
}

func GameTick() {
	for _, p := range ProducerMap {
		p.CalcProducer()

	}
	Builder.Calc()
	Researcher.Calc()

	Defense.Calc()
	Offensive.Calc()

	Enemy.Calc()

	GameTime += 100
}
