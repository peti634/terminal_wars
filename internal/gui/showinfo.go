package gui

import (
	"sort"
	"strings"

	"terminal_wars/internal/formater"
	"terminal_wars/internal/game"
	"terminal_wars/internal/info"
	"terminal_wars/internal/tviewadd"

	"github.com/rivo/tview"
)

type MenuShowInfoGroup struct {
	GroupName string
}

type MenuUnitInfo struct {
	InfoText string
}

var (
	middleInfoText *tviewadd.TextView2
	infoLogText    *tview.TextView

	selectedMenuUserData interface{}
)

func InitGameShowInfo() {
	infoLogText = tview.NewTextView().SetDynamicColors(true)
	info.SetInfoFunc(ShowInfo)
	info.Info("Welcome here")

	middleInfoText = tviewadd.NewTextView2()
	middleInfoText.SetDynamicColors(true)
	middleInfoText.SetTextDrawFunc(ShowProductInfo)
	middleInfoText.SetWordWrap(true)
}

func PrintEnum(title string, enums string) {
	if enums != "" {
		enums = enums[:len(enums)-1]
		info.Info(title + enums)
	}
}

func DepChangeFunc(dep string, gift []game.DepGift) {
	menuTreeContGame.RebuildActiveMenu()
	var newJob string
	var newBuilding string
	var newResearch string
	for _, p := range game.ProductMap {
		if p.NeedDep == dep {
			if p.Producer != nil {
				newJob += " " + p.Name + ","
			} else if p.Building != nil {
				newBuilding += " " + p.Name + ","
			} else if p.Research != nil {
				newResearch += " " + p.Name + ","
			}
		}
	}
	PrintEnum("New job avaliable: ", newJob)
	PrintEnum("New building available: ", newBuilding)
	PrintEnum("New research available: ", newResearch)

	var recvGift string
	for _, g := range gift {

		game.StoreMap[g.ProductName].Add(g.Value)
		recvGift += " " + g.ProductName + " (" + formater.ValueFormat(g.Value) + "),"
	}
	if dep == "DEP_BUILD_FACTORY" {
		info.Info("Research avaliable!!!!")
	}
	PrintEnum("You received a gift: ", recvGift)
}

func changeMenuItemFunc(m *MenuTree, e *MenuTreeItem) {
	if e != nil {
		selectedMenuUserData = e.UserData
	} else {
		if m.GetMenuUserData() != nil {
			selectedMenuUserData = m.GetMenuUserData()
		} else {
			selectedMenuUserData = nil
		}

	}

}

func GetProductCostString(p *game.Product) string {
	var ret string
	var youhaveret string
	ret = "Build cost:\r\n"
	ret += "Time: " + formater.TimeFormat(p.CostTime) + " s\r\n"

	for _, i := range p.CreateCost {
		ret += i.ProductName + ": " + formater.ValueFormat(i.Num) + "\r\n"
		youhaveret += i.ProductName + ": " +
			formater.ValueFormat(game.StoreMap[i.ProductName].GetCount()) + "\r\n"
	}
	if len(p.CreateCost) > 0 {
		ret += "\r\nYou have:\r\n" + youhaveret
	}

	return ret
}

func textToCenter(t *tviewadd.TextView2, text string) string {
	_, _, width, _ := middleInfoText.GetInnerRect()
	width = (width - len(text)) / 2
	if width > 0 {
		return strings.Repeat(" ", width) + text
	}
	return text
}

func textResourceInfoGroup(groupProduct string) string {
	var ret string

	productNameList := game.SearchProductNameByBuildingName(groupProduct)
	sort.Strings(productNameList)
	for _, name := range productNameList {
		product := game.ProductMap[name]
		if product.Producer != nil && game.CheckDep(product.NeedDep) {
			ret += name + ": " + game.ProducerMap[name].GetMoreInfo() + "\r\n"
		}
	}

	return ret
}
func textJobInfoGroup(groupProduct string) string {
	var ret string

	productNameList := game.SearchProductNameByBuildingName(groupProduct)
	sort.Strings(productNameList)
	for _, name := range productNameList {
		product := game.ProductMap[name]
		if product.Producer != nil && game.CheckDep(product.NeedDep) {
			producerNum := formater.ValueFormat(game.ProducerMap[name].GetProducerNum())
			ret += name + " job: " + producerNum + "\r\n"
		}
	}

	return ret
}

func textBuildNum() string {
	if game.CheckDep("DEP_RESC_MULTIPLE") == false {
		return ""
	}
	IfI := func(b bool, v1 string, v2 string) string {
		if b {
			return v1
		}
		return v2
	}

	var ret string
	ret += IfI(gameBuildNumIndex == 0, "1)[black:white]1[-:black] ", "1)1 ")
	ret += IfI(gameBuildNumIndex == 1, "2)[black:white]10[-:black] ", "2)10 ")
	ret += IfI(gameBuildNumIndex == 2, "3)[black:white]50[-:black] ", "3)50 ")
	ret += IfI(gameBuildNumIndex == 3, "4)[black:white]100[-:black] ", "4)100 ")

	return ret + "\n\n"
}

func ShowProductInfo(tv2 *tviewadd.TextView2) {
	var newText string

	if unitinfo, ok := selectedMenuUserData.(MenuUnitInfo); ok {
		newText += unitinfo.InfoText
	} else if groupInfo, ok := selectedMenuUserData.(MenuShowInfoGroup); ok {
		groupName := groupInfo.GroupName
		if groupName != "" {

			freeproducer := game.ProductMap[groupName].Building.ProducerAdd
			newText += textToCenter(tv2, groupName+" job info") + "\r\n"
			newText += "Free job: " + formater.ValueFormat(freeproducer.GetProducerNum()) + "\r\n"
			newText += "\r\n"
			newText += textJobInfoGroup(groupName) + "\r\n"
			newText += "\r\n"
			newText += textResourceInfoGroup(groupName) + "\r\n"

		}

	} else if producer, ok := selectedMenuUserData.(*game.Producer); ok {
		newText += textBuildNum()
		p := producer.GetProduct()
		newText += textToCenter(tv2, p.Name+" job info") + "\r\n"
		newText += "\r\n"
		newText += "Press [black:white:u]SPACE[-:black:-] increase this job\r\n"
		newText += "Press [black:white:u]BACKSPACE[-:black:-] decrease this job\r\n"
		newText += "\r\n"
		newText += "Worker here: " + formater.ValueFormat(producer.GetProducerNum()) + "\r\n"
		newText += "Free job: " + formater.ValueFormat(producer.GetFreeProducerNum()) + "\r\n"
		newText += "\r\nProduce:\r\n"
		newText += p.Name + ": " + producer.GetMoreInfo() + "\r\n"
		newText += "\r\n"
		newText += GetProductCostString(p)

	} else if p, ok := selectedMenuUserData.(*game.Product); ok {
		if p.Research != nil {
			newText += textToCenter(tv2, p.Name+" info") + "\r\n"
			if p.Research.CanReResearch {
				newText += "You have: " +
					formater.ValueFormat(p.CostIncreaser.BuildCounter) + " " + p.Name + "\r\n"
				newText += "Cost increase: " +
					formater.ValueFormat(p.CostIncreaser.IncFactor) + "% / " +
					formater.ValueFormat(p.CostIncreaser.IncStep) + "pc\r\n\r\n"
			}
			if p.Research.ShowText != "" {
				newText += p.Research.ShowText + "\r\n\r\n"
			}

		} else {
			newText += textBuildNum()
			newText += textToCenter(tv2, p.Name+" info") + "\r\n"
			newText += "You have: " +
				formater.ValueFormat(p.CostIncreaser.BuildCounter) + " " + p.Name + "\r\n"

			newText += "Cost increase: " +
				formater.ValueFormat(p.CostIncreaser.IncFactor) + "% / " +
				formater.ValueFormat(p.CostIncreaser.IncStep) + "pc\r\n"

			newText += "Cost decrease: " + formater.ValueFormat(p.CostIncreaser.DecFactor) + "%\r\n"
			if p.Building != nil && p.Building.ProducerAdd != nil {
				producernum := p.Building.ProducerAdd.GetProducerNum()
				newText += "Free job: " + formater.ValueFormat(producernum) + "\r\n"
			}
			newText += "\r\n"

		}

		newText += GetProductCostString(p)
	} else {
		newText += textToCenter(tv2, "Resource info") + "\r\n"

		newText += textResourceInfoGroup("Mine") + "\r\n"

		if game.CheckDep("DEP_BUILD_FURNACE") {
			newText += textResourceInfoGroup("Furnace") + "\r\n"
		}
		if game.CheckDep("DEP_BUILD_FACTORY") {
			newText += textResourceInfoGroup("Factory") + "\r\n"
		}
		if game.CheckDep("DEP_BUILD_ADVFACTORY") {
			newText += textResourceInfoGroup("Advanced factory") + "\r\n"
		}
		if game.CheckDep("DEP_BUILD_DEF") {
			newText += textResourceInfoGroup("Defense builder") + "\r\n"
		}
		if game.CheckDep("DEP_RESC_ARMY") {
			newText += textResourceInfoGroup("Army factory") + "\r\n"
		}
	}
	//newText += game.Enemy.GetDebugInfo()
	middleInfoText.SetText(newText)
}

func ShowInfo(s string) {
	infoLogText.SetText(infoLogText.GetText(false) + "\r" + s)
}
