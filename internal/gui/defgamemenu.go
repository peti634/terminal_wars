package gui

import (
	"math/rand"

	"terminal_wars/internal/formater"
	"terminal_wars/internal/game"
	"terminal_wars/internal/tviewadd"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

func GameMenuInputFunc(event *tcell.EventKey) *tcell.EventKey {
	if event.Rune() == 32 || event.Rune() == 127 { //Space and Backspace
		item := menuTreeContGame.GetCurrentItem()
		if item != nil {
			p, ok := item.UserData.(*game.Producer)
			if ok {
				if event.Rune() == 32 {
					p.AddProducer(gameBuildNum)
				} else {
					p.DecProducer(gameBuildNum)
				}

			}
		}

		return nil
	}
	if game.CheckDep("DEP_RESC_MULTIPLE") {
		if event.Rune() == 49 {
			gameBuildNumIndex = 0
			gameBuildNum = 1
		}
		if event.Rune() == 50 {
			gameBuildNumIndex = 1
			gameBuildNum = 10
		}
		if event.Rune() == 51 {
			gameBuildNumIndex = 2
			gameBuildNum = 50
		}
		if event.Rune() == 52 {
			gameBuildNumIndex = 3
			gameBuildNum = 100
		}
	} else {
		gameBuildNumIndex = 0
		gameBuildNum = 1
	}
	//info.Info(fmt.Sprint(event.Rune()))
	return event
}
func BuyProduct(m *MenuTree, e *MenuTreeItem) {
	p, ok := e.UserData.(*game.Product)
	if ok {
		for i := 0; i < gameBuildNum; i++ {
			game.AddToCreator(p)
		}

	}
}

func SendArmyToAttack(m *MenuTree, e *MenuTreeItem) {
	var attackers []game.AttackerArmyGroup
	if count := game.StoreMap["Robot warrior"].GetCount(); count > 0 {
		attackers = append(attackers,
			game.AttackerArmyGroup{
				Count: count,
				Army:  game.ProductMap["Robot warrior"],
			})
		game.StoreMap["Robot warrior"].Add(-count)
	}
	if count := game.StoreMap["Tank"].GetCount(); count > 0 {
		attackers = append(attackers,
			game.AttackerArmyGroup{
				Count: count,
				Army:  game.ProductMap["Tank"],
			})
		game.StoreMap["Tank"].Add(-count)
	}
	if len(attackers) == 0 {
		return
	}

	game.Offensive.AddAttacker(
		rand.Intn(game.PLAYER_ATTACKTIME_FROM)+game.PLAYER_ATTACKTIME_RANGE,
		attackers)
}

func InitGameMenu() *MenuTree {
	/////////////////////MENU/////////////////////////

	createGameMenuFromProductArray := func(arr []string) *MenuTree {
		ret := make([]MenuTreeItem, len(arr))
		for i, s := range arr {
			p := game.ProductMap[s]

			if p.Producer != nil { //produced product check
				ret[i] = MenuTreeItem{
					Text:     "Job for: " + p.Name,
					UserData: game.ProducerMap[s],
				}
			} else if p.Building != nil { //building check
				ret[i] = MenuTreeItem{
					Text:       "Build " + p.Name,
					UserData:   p,
					SelectFunc: BuyProduct,
				}
			} else {
				//error
			}
			if p.NeedDep != "" {
				ret[i].Dep = p.NeedDep
			}

		}
		return NewMenu(ret)
	}

	minemenu := createGameMenuFromProductArray(
		[]string{"Mine", "Iron", "Copper", "Coal", "Silicon"})

	furnacemenu := createGameMenuFromProductArray(
		[]string{
			"Furnace",
			"Iron plate",
			"Copper plate",
			"Gunpowder",
			"Silicon plate",
			"Iron block",
		})

	factorymenu := createGameMenuFromProductArray(
		[]string{
			"Factory",
			"Copper cable",
			"Bulb",
			"Basic lab pack",
			"Screw",
			"Circuit",
			"Improve lab pack",
		})
	advfactorymenu := createGameMenuFromProductArray(
		[]string{"Advanced factory", "Engine"})
	storemenu := createGameMenuFromProductArray(
		[]string{"Mine store", "Furnace store", "Factory store", "Adv Factory store"})

	deffmenu := createGameMenuFromProductArray(
		[]string{"Defense builder", "Raw ammo", "Wall repair", "Turret", "Turret ammo"})

	armymenu := createGameMenuFromProductArray(
		[]string{"Army factory", "Robot warrior", "Tank", "Generator", "Artillery", "Buffer"})
	buildingsmenu := NewMenu([]MenuTreeItem{
		MenuTreeItem{
			Text:           "Mine",
			EnterMenu:      minemenu,
			UserData:       MenuShowInfoGroup{GroupName: "Mine"},
			UDataEnterMenu: true,
		},
		MenuTreeItem{
			Text:           "Furnace",
			EnterMenu:      furnacemenu,
			UserData:       MenuShowInfoGroup{GroupName: "Furnace"},
			UDataEnterMenu: true,
			Dep:            "DEP_BUILD_MINE",
		},
		MenuTreeItem{
			Text:           "Factory",
			EnterMenu:      factorymenu,
			UserData:       MenuShowInfoGroup{GroupName: "Factory"},
			UDataEnterMenu: true,
			Dep:            "DEP_BUILD_FURNACE",
		},
		MenuTreeItem{
			Text:           "Advanced factory",
			EnterMenu:      advfactorymenu,
			UserData:       MenuShowInfoGroup{GroupName: "Advanced factory"},
			UDataEnterMenu: true,
			Dep:            "DEP_RESC_ADVFACTORY",
		},
		MenuTreeItem{
			Text:           "Store",
			EnterMenu:      storemenu,
			UserData:       MenuShowInfoGroup{GroupName: ""},
			UDataEnterMenu: true,
			Dep:            "DEP_RESC_STORE",
		},
		MenuTreeItem{
			Text:           "Defend",
			EnterMenu:      deffmenu,
			UserData:       MenuShowInfoGroup{GroupName: "Defense builder"},
			UDataEnterMenu: true,
			Dep:            "DEP_RESC_DEF",
		},
		MenuTreeItem{
			Text:           "Army",
			EnterMenu:      armymenu,
			UserData:       MenuShowInfoGroup{GroupName: "Army factory"},
			UDataEnterMenu: true,
			Dep:            "DEP_RESC_ARMY",
		},
	})

	armymainmenu := NewMenu([]MenuTreeItem{

		MenuTreeItem{
			Text:       "Send army to attack",
			SelectFunc: SendArmyToAttack,
		},
	})

	gameinfomenu := NewMenu([]MenuTreeItem{
		MenuTreeItem{
			Text: "Unit info: Robot warrior",
			UserData: MenuUnitInfo{
				InfoText: "The Robot warrior the first unit. It is a simple cheap unit with few health and damage" +
					"\r\n" +
					"\r\nDamage: " + formater.ValueFormat(game.ProductMap["Robot warrior"].Army.Damage) +
					"\r\nHealth: " + formater.ValueFormat(game.ProductMap["Robot warrior"].Army.Health) +
					"\r\nPriority: " + formater.ValueFormat(game.ProductMap["Robot warrior"].Army.Priority),
			},
		},
		MenuTreeItem{
			Text: "Unit info: Tank",
			UserData: MenuUnitInfo{
				InfoText: "The Tank the second unit. It is a heavy unit, not cheap but it have middle health and damage" +
					"\r\n" +
					"\r\nDamage: " + formater.ValueFormat(game.ProductMap["Tank"].Army.Damage) +
					"\r\nHealth: " + formater.ValueFormat(game.ProductMap["Tank"].Army.Health) +
					"\r\nPriority: " + formater.ValueFormat(game.ProductMap["Tank"].Army.Priority),
			},
		},
	})

	gamemenu := NewMenu([]MenuTreeItem{
		MenuTreeItem{
			Text:      "Buildings",
			EnterMenu: buildingsmenu,
		},
		MenuTreeItem{
			Text:      "Researches",
			EnterMenu: GetMenuResearch(),
			Dep:       "DEP_BUILD_FACTORY",
		},
		MenuTreeItem{
			Text:      "Army",
			EnterMenu: armymainmenu,
			Dep:       "DEP_RESC_ARMY",
		},
		MenuTreeItem{
			Text:      "Info",
			EnterMenu: gameinfomenu,
			Dep:       "DEP_RESC_INFO",
		},
		MenuTreeItem{
			Text: "Pause",
			SelectFunc: func(m *MenuTree, e *MenuTreeItem) {
				GameModalPause(modalPause)
			},
		},
	})

	return gamemenu
}

func InitGameTopInfo() (generic tview.Primitive, def tview.Primitive, off tview.Primitive) {
	genericInfoFlex := tview.NewFlex().SetDirection(tview.FlexRow)
	defenseInfoFlex := tview.NewFlex().SetDirection(tview.FlexRow)
	offensiveInfoFlex := tview.NewFlex().SetDirection(tview.FlexRow)

	AddInfoText := func(flex *tview.Flex, getInfoFunc func() string) {

		flex.AddItem(
			tviewadd.NewTextView2().
				SetDynamicColors(true).
				SetTextDrawFunc(func(tv2 *tviewadd.TextView2) {
					tv2.SetText(getInfoFunc())
				}), 1, 0, false)
	}

	AddInfoText(genericInfoFlex, func() string {
		return "Game time: " + formater.TimeFormat(game.GameTime)
	})
	AddInfoText(genericInfoFlex, func() string {
		return "Base health: " + game.Defense.GetBaseInfo()
	})
	AddInfoText(genericInfoFlex, func() string {
		return "Building: " + game.Builder.GetInfo()
	})
	AddInfoText(genericInfoFlex, func() string {
		return "Researching: " + game.Researcher.GetInfo()
	})
	AddInfoText(genericInfoFlex, func() string {
		return game.Defense.GetAttackTimeInfo("Enemy will attack in: ")
	})
	AddInfoText(genericInfoFlex, func() string {
		return game.Offensive.GetAttackTimeInfo("We will attack in: ")
	})

	offensiveInfoFlex.AddItem(
		tview.NewTextView().
			SetText("Offensive").
			SetTextAlign(tview.AlignCenter), 1, 0, false)
	AddInfoText(offensiveInfoFlex, func() string {
		if game.Offensive.AttackingProgressing() == false {
			return ""
		}
		return "Enemy Base health: " + game.Offensive.GetBaseInfo()
	})
	AddInfoText(offensiveInfoFlex, func() string {
		if game.Offensive.UnderAttack() == false {
			return ""
		}
		return "Enemy Wall health: " + game.Offensive.GetWallInfo()
	})

	AddInfoText(offensiveInfoFlex, func() string {
		if game.Offensive.UnderAttack() == false {
			return ""
		}
		return "Enemy Turret: " + game.Offensive.GetTurretNumInfo()
	})

	AddInfoText(offensiveInfoFlex, func() string {
		if game.Offensive.UnderAttack() == false {
			return ""
		}
		return "Enemy Turret target: " + game.Offensive.GetTargetInfo()
	})
	AddInfoText(offensiveInfoFlex, func() string {
		if game.Offensive.AttackingProgressing() == false {
			return ""
		}
		return game.Offensive.GetArmyInfo()
	})

	defenseInfoFlex.AddItem(
		tview.NewTextView().
			SetText("Defense").
			SetTextAlign(tview.AlignCenter), 1, 0, false)

	AddInfoText(defenseInfoFlex, func() string {
		if game.CheckDep("DEP_RESC_DEFWALL") == false {
			return ""
		}
		return "Wall health: " + game.Defense.GetWallInfo()
	})

	AddInfoText(defenseInfoFlex, func() string {
		if game.CheckDep("DEP_RESC_DEFTURR") == false {
			return ""
		}
		return "Turret: " + game.Defense.GetTurretNumInfo()
	})

	AddInfoText(defenseInfoFlex, func() string {
		if game.CheckDep("DEP_RESC_DEFFAMMO") == false {
			return ""
		}
		return "Turret ammo: " + game.Defense.GetTurretAmmoInfo()
	})
	AddInfoText(defenseInfoFlex, func() string {
		if game.Defense.UnderAttack() == false {
			return ""
		}
		return "Turret target: " + game.Defense.GetTargetInfo()
	})
	AddInfoText(defenseInfoFlex, func() string {
		if game.Defense.AttackingProgressing() == false {
			return ""
		}
		return game.Defense.GetArmyInfo()
	})

	return genericInfoFlex, defenseInfoFlex, offensiveInfoFlex
}

func initModals() {
	defaultPauseTitle := "Game is paused!"
	gamePauseEventFunc := func(buttonIndex int, buttonLabel string) {
		if buttonIndex == 0 { //Continue
			GameContinue()
			modalPause.SetText(defaultPauseTitle)
		}
		if buttonIndex == 1 { //Continue
			mapname := saveGame()
			modalPause.SetText("Game is paused!\n\nGame is saved:\n" + mapname)
		}

		if buttonIndex == 2 { //Exit
			GameExit()
			modalPause.SetText(defaultPauseTitle)
		}
	}

	modalPause = tview.NewModal()
	modalPause.SetText(defaultPauseTitle).
		AddButtons([]string{"c) Continue", "s) Game save", "e) Exit"}).
		SetDoneFunc(gamePauseEventFunc)
	modalPause.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Rune() == 'e' {
			gamePauseEventFunc(2, "")
		}
		if event.Rune() == 's' {
			gamePauseEventFunc(1, "")
		}
		if event.Rune() == 'c' {
			gamePauseEventFunc(0, "")
		}
		return event
	})

	modalWin = tview.NewModal()
	modalWin.SetText("You win!").
		AddButtons([]string{"e) Exit"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			if buttonIndex == 0 { //Exit
				GameExit()
			}
		})
	modalWin.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Rune() == 'e' {
			GameExit()
		}
		return event
	})
	modalLost = tview.NewModal()
	modalLost.SetText("You lost!").
		AddButtons([]string{"e) Exit"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			if buttonIndex == 0 { //Exit
				GameExit()
			}
		})
	modalLost.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Rune() == 'e' {
			GameExit()
		}
		return event
	})
}
