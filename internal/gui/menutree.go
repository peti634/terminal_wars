package gui

import (
	"strconv"

	"github.com/rivo/tview"
)

type MenuTreeContainer struct {
	refList          *tview.List
	selectChangeFunc func(m *MenuTree, e *MenuTreeItem)
	depCheckFunc     func(Dep string) bool
	activeMenu       *MenuTree
}

type MenuTree struct {
	name       string
	elements   []MenuTreeItem
	parentMenu *MenuTree
	UserData   interface{}
}

type MenuTreeItem struct {
	Text       string
	SelectFunc func(m *MenuTree, e *MenuTreeItem)
	EnterMenu  *MenuTree
	UserData   interface{}
	//store user data for enter menu when enter
	UDataEnterMenu bool

	//List of dependencies
	Dep     string
	HideDep string
}

//////////////////////MenuTree//////////////////
func NewMenu(elements []MenuTreeItem) *MenuTree {

	r := new(MenuTree)
	r.elements = elements
	r.parentMenu = nil

	return r
}

func (m *MenuTree) ResetElements(elements []MenuTreeItem) {
	m.elements = elements
}
func (m *MenuTree) SetName(s string) {
	m.name = s
}

func (m *MenuTree) GetName() string {
	return m.name
}

func (m *MenuTree) SetParentMenu(p *MenuTree) {
	m.parentMenu = p
}

func (m *MenuTree) GetMenuUserData() interface{} {
	return m.UserData
}

//////////////////////MenuTreeContainer//////////////////
func NewMenuTreeContainer(
	selectChangeFunc func(m *MenuTree, e *MenuTreeItem),
	depCheckFunc func(Dep string) bool) *MenuTreeContainer {

	return &MenuTreeContainer{
		refList:          tview.NewList().ShowSecondaryText(false),
		selectChangeFunc: selectChangeFunc,
		depCheckFunc:     depCheckFunc,
	}
}

func (mtc *MenuTreeContainer) GetList() *tview.List {
	return mtc.refList
}

func (mtc *MenuTreeContainer) SetSelectChangeFunc(f func(m *MenuTree, e *MenuTreeItem)) {
	mtc.selectChangeFunc = f
}

func (mtc *MenuTreeContainer) SetDepCheckFunc(f func(Dep string) bool) {
	mtc.depCheckFunc = f
}

func (mtc *MenuTreeContainer) RebuildActiveMenu() {
	mtc.Rebuild()
}

func (mtc *MenuTreeContainer) GetCurrentItem() *MenuTreeItem {
	_, secText := mtc.refList.GetItemText(mtc.refList.GetCurrentItem())
	return mtc.activeMenu.GetElementByIndex(secText)
}

func (m *MenuTree) GetElementByIndex(s string) *MenuTreeItem {
	i, err := strconv.Atoi(s)
	if err != nil {
		return nil
	}
	return &m.elements[i]
}

func (mtc *MenuTreeContainer) onClick(i int, s1 string, s2 string, r rune) {
	e := mtc.activeMenu.GetElementByIndex(s2)
	if e == nil {
		mtc.Activate(mtc.activeMenu.parentMenu)
		return
	}
	if e.EnterMenu != nil && e.UDataEnterMenu {
		e.EnterMenu.UserData = e.UserData
	}
	if e.SelectFunc != nil {
		e.SelectFunc(mtc.activeMenu, e)
	}
	if e.EnterMenu != nil {
		e.EnterMenu.SetParentMenu(mtc.activeMenu)
		mtc.Activate(e.EnterMenu)
	}

}

func (mtc *MenuTreeContainer) onSelectChange(i int, s1 string, s2 string, r rune) {
	e := mtc.activeMenu.GetElementByIndex(s2)
	if mtc.selectChangeFunc != nil {
		if e == nil {
			mtc.selectChangeFunc(mtc.activeMenu, nil)
		} else {
			mtc.selectChangeFunc(mtc.activeMenu, e)
		}
	}

}

func (mtc *MenuTreeContainer) buildItems() {
	mtc.refList.Clear()

	if mtc.activeMenu.parentMenu != nil {
		mtc.refList.AddItem("<-Back", "", 'q', nil)
	}
	startShort := 'a'
	for i, e := range mtc.activeMenu.elements {

		if e.HideDep != "" && mtc.depCheckFunc(e.HideDep) {
			continue
		}
		if e.Dep != "" {
			if mtc.depCheckFunc(e.Dep) == false {
				continue
			}

		}
		mtc.refList.AddItem(e.Text, strconv.Itoa(i), startShort, nil)
		startShort++
	}
}

func (mtc *MenuTreeContainer) Activate(m *MenuTree) {
	mtc.activeMenu = m

	mtc.refList.SetSelectedFunc(mtc.onClick)
	mtc.refList.SetChangedFunc(mtc.onSelectChange)

	mtc.buildItems()
}

func (mtc *MenuTreeContainer) Rebuild() {
	firstText, secText := mtc.refList.GetItemText(mtc.refList.GetCurrentItem())

	mtc.buildItems()

	findList := mtc.refList.FindItems(firstText, secText, true, false)
	if len(findList) > 0 {
		mtc.refList.SetCurrentItem(findList[0])
	}
}
