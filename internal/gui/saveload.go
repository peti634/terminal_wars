package gui

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strconv"
	"time"

	"terminal_wars/internal/game"
)

type FileMap struct {
	RealTime     int64 `json:",omitempty"`
	GameTime     int   `json:",omitempty"`
	Campinglevel int   `json:",omitempty"`

	Enemy        game.EnemyAIFile
	Defense      game.AttackerFile
	Offensive    game.AttackerFile
	Builder      game.CreatorFile
	Researcher   game.CreatorFile
	Dep          map[string]bool
	Store        map[string]game.StoreFile
	Product      map[string]game.ProductFile
	Producer     map[string]game.ProducerFile
	FreeProducer map[string]game.ProducerFile
}

type FileMapMenuUData struct {
	Map FileMap
}

type FileStruct struct {
	Version         int      `json:",omitempty"`
	Options         struct{} `json:",omitempty"`
	UnlockCampLevel int      `json:",omitempty"`
	Checksum        int      `json:",omitempty"`
	SavedMapList    []FileMap
}

var (
	LoadSaveData FileStruct
)

func initFileStruct() FileStruct {
	return FileStruct{
		Version:      1000,
		Checksum:     0,
		SavedMapList: []FileMap{},
	}
}

func getMapTitle(m FileMap) string {
	return time.Unix(m.RealTime, 0).Format("2006-01-02 3:4:5 PM") + " Map: " + strconv.Itoa(m.Campinglevel+1)
}
func saveCampingFinish(lvl int) {
	if LoadSaveData.UnlockCampLevel < lvl {
		LoadSaveData.UnlockCampLevel = lvl
		saveDataToFile()
	}
}

func saveGame() string {

	filemap := FileMap{
		GameTime:     game.GameTime,
		Enemy:        game.Enemy.SaveData(),
		RealTime:     time.Now().Unix(),
		Store:        map[string]game.StoreFile{},
		Product:      map[string]game.ProductFile{},
		Producer:     map[string]game.ProducerFile{},
		FreeProducer: map[string]game.ProducerFile{},
		Researcher:   game.Researcher.SaveData(),
		Builder:      game.Builder.SaveData(),
		Defense:      game.Defense.SaveData(),
		Offensive:    game.Offensive.SaveData(),
		Dep:          game.DepSaveData(),
	}

	for name, i := range game.FreeProducerMap {
		filemap.FreeProducer[name] = i.SaveData()
	}

	for name, i := range game.StoreMap {
		filemap.Store[name] = i.SaveData()
	}

	for name, i := range game.ProductMap {
		filemap.Product[name] = i.SaveData()
	}

	for name, i := range game.ProducerMap {
		filemap.Producer[name] = i.SaveData()
	}
	LoadSaveData.SavedMapList = append([]FileMap{filemap}, LoadSaveData.SavedMapList...)
	if len(LoadSaveData.SavedMapList) > 10 {
		LoadSaveData.SavedMapList = LoadSaveData.SavedMapList[:10]
	}

	saveDataToFile()
	return getMapTitle(filemap)
}

func saveDataToFile() {
	file, _ := json.MarshalIndent(LoadSaveData, "", "	")

	err := ioutil.WriteFile("twsave.data", file, 0777)

	if err != nil {
		panic(err)
	}
}
func ReadAndLoadSavedData() {
	success := true
	files := initFileStruct()
	file, readerr := ioutil.ReadFile("twsave.data")
	if os.IsNotExist(readerr) {
		success = false
	} else {

		err := json.Unmarshal([]byte(file), &files)
		if err == nil {
			LoadSaveData = files
		} else {
			success = false
		}
	}

	if success == false {
		LoadSaveData = initFileStruct()
		saveDataToFile()
	}

}

func AppplyLoadSavedGame(filemap FileMap) {

	game.GameTime = filemap.GameTime
	game.Enemy.LoadData(filemap.Enemy)
	game.Researcher.LoadData(filemap.Researcher)
	game.Builder.LoadData(filemap.Builder)
	game.Defense.LoadData(filemap.Defense)
	game.Offensive.LoadData(filemap.Offensive)
	game.DepLoadData(filemap.Dep)

	for name, i := range filemap.FreeProducer {
		game.FreeProducerMap[name].LoadData(i)
	}

	for name, i := range filemap.Store {
		game.StoreMap[name].LoadData(i)
	}

	for name, i := range filemap.Product {
		game.ProductMap[name].LoadData(i)
	}

	for name, i := range filemap.Producer {
		game.ProducerMap[name].LoadData(i)
	}

}

func SelectSavedGameFunc(m *MenuTree, e *MenuTreeItem) {
	if filemap, ok := e.UserData.(FileMapMenuUData); ok {
		StartGameLoad(filemap.Map)
	}
}

func BuildLoadMenuList(m *MenuTree, e *MenuTreeItem) {
	newMenuItems := []MenuTreeItem{}

	for _, v := range LoadSaveData.SavedMapList {
		newMenuItems = append(newMenuItems,
			MenuTreeItem{
				Text:       getMapTitle(v),
				UserData:   FileMapMenuUData{Map: v},
				SelectFunc: SelectSavedGameFunc,
			},
		)
	}

	menuTreeLoadGame.ResetElements(newMenuItems)
}
