package gui

import (
	"terminal_wars/internal/game"
)

func generateMenuResearch(arr []string) *MenuTree {
	ret := make([]MenuTreeItem, len(arr))
	for i, s := range arr {
		p := game.ProductMap[s]

		if p.Research != nil { //produced product check
			ret[i] = MenuTreeItem{
				Text:       "Research: " + p.Name,
				UserData:   p,
				SelectFunc: BuyProduct,
				HideDep:    p.Research.DepResolver,
			}
			if p.Research.CanReResearch == false {
				ret[i].HideDep = p.Research.DepResolver
			}
			if p.NeedDep != "" {
				ret[i].Dep = p.NeedDep
			}
		} else {
			//error
		}

	}
	return NewMenu(ret)
}

func GetMenuResearch() *MenuTree {

	fastresearchmenu := generateMenuResearch(
		[]string{
			"Fast research resch",
			"Fast build resch",
			"Fast mine resch",
			"Fast furnace resch",
			"Fast factory resch",
			"Fast adv factory resch",
			"Fast defense resch",
			"Fast army resch",
		})

	cheaperresearchmenu := generateMenuResearch(
		[]string{
			"Cheaper mine resch",
			"Cheaper furnace resch",
			"Cheaper factory resch",
			"Cheaper adv factory resch",
			"Cheaper store resch",
			"Cheaper defense resch",
			"Cheaper army resch",
		})

	newresearchmenu := generateMenuResearch(
		[]string{
			"Info menu resch",
			"Store resch",
			"Multiple build resch",
			"Silicon mine resch",
			"Improve lab pack resch",
			"Improving resch",
			"Small upgrade resch",
			"Defense resch",
			"Army resch",
			"Adv factory resch",
		})

	defresearchmenu := generateMenuResearch(
		[]string{
			"Wall resch",
			"Turret resch",
			"Turret ammo resch",
			"Inc. Wall maxhp resch",
		})

	armyresearchmenu := generateMenuResearch(
		[]string{
			"Robot resch",
			"Tank resch",
		})

	return NewMenu([]MenuTreeItem{
		MenuTreeItem{
			Text:      "Research: Faster",
			EnterMenu: fastresearchmenu,
			Dep:       "DEP_RESC_IMPROV",
		},
		MenuTreeItem{
			Text:      "Research: Cheaper",
			EnterMenu: cheaperresearchmenu,
			Dep:       "DEP_RESC_IMPROV",
		},
		MenuTreeItem{
			Text:      "New research",
			EnterMenu: newresearchmenu,
		},
		MenuTreeItem{
			Text:      "Defensive research",
			Dep:       "DEP_RESC_DEF",
			EnterMenu: defresearchmenu,
		},
		MenuTreeItem{
			Text:      "Army research",
			Dep:       "DEP_RESC_ARMY",
			EnterMenu: armyresearchmenu,
		},
	})
}
