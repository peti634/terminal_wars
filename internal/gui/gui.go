package gui

import (
	"time"

	"terminal_wars/internal/game"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

//https://patorjk.com/software/taag/#p=display&f=Calvin%20S&t=TERMINAL%20WARS
const logo = `
╔╦╗╔═╗╦═╗╔╦╗╦╔╗╔╔═╗╦    ╦ ╦╔═╗╦═╗╔═╗
 ║ ║╣ ╠╦╝║║║║║║║╠═╣║    ║║║╠═╣╠╦╝╚═╗
 ╩ ╚═╝╩╚═╩ ╩╩╝╚╝╩ ╩╩═╝  ╚╩╝╩ ╩╩╚═╚═╝`

const appVerisonTitle = `V0.1 2020
Created by Iván Péter (peti634)
Pokol studio
http://www.pokolstudio.hu/
`

const refreshInterval = 100 * time.Millisecond

var (
	app       *tview.Application
	appScreen tcell.Screen

	gamePrimiteve     tview.Primitive
	mainmenuPrimiteve tview.Primitive

	mainflex *tview.Flex
	gridGame *tview.Grid

	menuTreeContMainMenu *MenuTreeContainer
	menuTreeContGame     *MenuTreeContainer
	menuTreeLoadGame     *MenuTree

	gameIsPause bool
	gameIsStop  bool

	gameBuildNum      int = 1
	gameBuildNumIndex int = 0

	modalPause *tview.Modal
	modalWin   *tview.Modal
	modalLost  *tview.Modal
)

func GameGuiInit() {

	mainflex = tview.NewFlex().SetFullScreen(true)
	gridGame = tview.NewGrid()

	menuTreeContGame = NewMenuTreeContainer(changeMenuItemFunc, game.CheckDep)
	menuTreeContGame.Activate(InitGameMenu())

	menuTreeContGame.GetList().SetInputCapture(GameMenuInputFunc)

	genericInfo, defInfo, offInfo := InitGameTopInfo()

	gridGame.SetRows(6).
		SetColumns(40, 35).
		SetBorders(true).
		AddItem(genericInfo, 0, 0, 1, 1, 0, 0, false).
		AddItem(defInfo, 0, 1, 1, 1, 0, 0, false).
		AddItem(offInfo, 0, 2, 1, 1, 0, 0, false)

	InitGameShowInfo()

	gridGame.AddItem(menuTreeContGame.GetList(), 1, 0, 1, 1, 0, 0, true).
		AddItem(middleInfoText, 1, 1, 1, 1, 0, 0, false).
		AddItem(infoLogText, 1, 2, 1, 1, 0, 0, false)

	mainflex.AddItem(gridGame, 0, 100, true)
	gamePrimiteve = mainflex
}

func GameContinue() {
	mainflex.Clear()
	mainflex.AddItem(gridGame, 0, 100, true)
	app.SetFocus(mainflex)
	appScreen.Clear()
	gameIsPause = false
}
func GameModalPause(modal *tview.Modal) {
	mainflex.Clear()
	mainflex.AddItem(gridGame, 0, 100, false)
	mainflex.AddItem(modal, 0, 0, true)
	app.SetFocus(modal)
	gameIsPause = true
}
func GameExit() {
	gameIsStop = true
	app.SetRoot(mainmenuPrimiteve, true)
}
func GameTick() {

	for gameIsStop == false {
		if gameIsPause == false {
			time.Sleep(refreshInterval)
			game.GameTick()
			app.Draw()
		}
	}
}

func WinLoseFunc(a *game.Attacker) {
	if a == game.Offensive { //Win
		GameModalPause(modalWin)
	} else { //LOST
		GameModalPause(modalLost)
	}
}

func StartGameLoad(filemap FileMap) {
	gameIsStop = false
	GameContinue()
	game.GameStateReset()
	AppplyLoadSavedGame(filemap)
	app.SetRoot(gamePrimiteve, true)
	menuTreeContGame.RebuildActiveMenu()
	go GameTick()
}

func StartGameCamping(m *MenuTree, e *MenuTreeItem) {
	gameIsStop = false
	GameContinue()
	game.GameStateReset()
	app.SetRoot(gamePrimiteve, true)
	go GameTick()
}

func GuiInit() {

	ReadAndLoadSavedData()
	game.GameInit()
	game.Defense.SetLoseBaseFunc(WinLoseFunc)
	game.Offensive.SetLoseBaseFunc(WinLoseFunc)

	app = tview.NewApplication()
	appScreen, _ = tcell.NewScreen()
	appScreen.Init()
	app.SetScreen(appScreen)

	mainMenuFlex := tview.NewFlex()
	mainmenuPrimiteve = mainMenuFlex

	GameGuiInit()
	initModals()

	gamemainmenu := NewMenu([]MenuTreeItem{
		MenuTreeItem{
			Text:       "Campaign 1",
			SelectFunc: StartGameCamping,
		},
	})
	menuTreeLoadGame = NewMenu([]MenuTreeItem{})
	welcommainmenu := NewMenu([]MenuTreeItem{
		MenuTreeItem{
			Text:      "Campaigns",
			EnterMenu: gamemainmenu,
		},
		MenuTreeItem{
			Text:       "Load game",
			EnterMenu:  menuTreeLoadGame,
			SelectFunc: BuildLoadMenuList,
		},
		MenuTreeItem{
			Text: "Quit",
			SelectFunc: func(m *MenuTree, e *MenuTreeItem) {
				app.Stop()
			},
		},
	})
	menuTreeContMainMenu = NewMenuTreeContainer(changeMenuItemFunc, game.CheckDep)

	mainMenuFlex.
		AddItem(tview.NewBox(), 0, 50, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(tview.NewTextView().SetText(logo), 6, 0, false).
			AddItem(menuTreeContMainMenu.GetList(), 0, 80, true).
			AddItem(tview.NewTextView().SetText(appVerisonTitle), 5, 0, false), 40, 0, true).
		AddItem(tview.NewBox(), 0, 50, false)

	game.DepSetChangeDepFunc(DepChangeFunc)
	menuTreeContMainMenu.Activate(welcommainmenu)
	app.SetRoot(mainMenuFlex, true)
}

func GuiAppRun() {

	if err := app.Run(); err != nil {
		panic(err)
	}
}
